﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovationGroup.Data.DataModels
{
    [Table("Employees")]
    public class Employee
    {
        public Employee()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeId { get; set; }

        [Required] [MaxLength(30)] public string LastName { get; set; }

        [Required]
        [MinLength(10), MaxLength(30)]
        public string FirstName { get; set; }

        [MaxLength(30)] public string JobTitle { get; set; }

        [MaxLength(30)] public string TitleOfCourtesy { get; set; }

        public DateTime? BirthDate { get; set; }

        public DateTime? HireDate { get; set; }

        [MaxLength(60)] public string Address { get; set; }

        [MaxLength(15)] public string City { get; set; }

        [MaxLength(15)] public string Region { get; set; }

        [MaxLength(10)] public string PostCode { get; set; }

        [MaxLength(15)] public string Country { get; set; }

        [MaxLength(20)] public string HomePhone { get; set; }

        [MaxLength(4)] public string Extension { get; set; }

        public byte[] Photo { get; set; }

        public string Notes { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
