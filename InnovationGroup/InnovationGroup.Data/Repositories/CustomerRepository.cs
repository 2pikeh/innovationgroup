﻿using System;
using System.Linq;
using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Customers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace InnovationGroup.Data.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        private readonly IPagedDatasetService _pagingService;
        private readonly IPropertyMappingService _propertyMappingService;

        public CustomerRepository(IInnovationGroupContext dbContext, 
            IPagedDatasetService pagingService, IPropertyMappingService propertyMappingService) 
            : base(dbContext)
        {
            _pagingService = pagingService;
            _propertyMappingService = propertyMappingService;
        }

        public async Task<Customer> GetByIdAsync(string id, string navigationPropertiesToInclude = null)
        {
            var customer = await GetEntitySet().FirstOrDefaultAsync(c =>
                string.Equals(c.CustomerId, id, StringComparison.InvariantCultureIgnoreCase));

            var dtoToEntityTypeMappings = _propertyMappingService.GetPropertyMapping<CustomersDto, Customer>();
            var customerProperties = customer.GetType().GetProperties();
            var navigationProperties = navigationPropertiesToInclude?.Split(",");

            if (navigationProperties == null)
                return customer;

            foreach (var navigationProperty in navigationProperties)
            {
                var entityPropertyFromMappings =
                    dtoToEntityTypeMappings[navigationProperty].DestinationProperties.First();

                if (!customerProperties.Any(p => string.Equals(p.Name, entityPropertyFromMappings,
                    StringComparison.InvariantCultureIgnoreCase)))
                    continue;

                if (Entry(customer).Member(entityPropertyFromMappings) is ReferenceEntry reference)
                {
                    reference.Load();
                }
                else
                {
                    await Entry(customer).Navigation(entityPropertyFromMappings).LoadAsync();
                }
            }

            return customer;
        }

        public async Task<bool> CustomerExistsAsync(string id) =>
            await GetEntitySet()
                .AnyAsync(c => string.Equals(c.CustomerId, id, StringComparison.InvariantCultureIgnoreCase));

        public async Task<int> DeleteAsync(string id)
        {
            Customer entity = await GetByIdAsync(id);
            GetEntitySet().Remove(entity);

            return await SaveChangesAsync();
        }

        public PagedDataset<Customer> GetAll(CustomerResourceQueryParameters queryParameters) =>
            _pagingService.ApplyPaging(GetAll(), queryParameters.PageNumber, queryParameters.PageSize);
    }
}
