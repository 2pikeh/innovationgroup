﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface IProductsRepository : IRepository<Product>
    {
        Task<Product> GetByIdAsync(int id);

        Task<bool> ProductExistsAsync(int id);

        Task<int> DeleteAsync(int id);
    }
}
