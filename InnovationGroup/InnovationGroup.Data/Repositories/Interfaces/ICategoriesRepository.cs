﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Services;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface ICategoriesRepository : IRepository<Category>
    {
        Task<bool> CategoryExistsAsync(int id);

        Task<Category> GetByIdAsync(int id);

        Task<int> DeleteAsync(int id);

        PagedDataset<Category> GetAll(ResourceQueryParameters queryParameters);
    }
}
