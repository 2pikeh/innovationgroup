﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Customers;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<bool> CustomerExistsAsync(string id);

        Task<int> DeleteAsync(string id);

        PagedDataset<Customer> GetAll(CustomerResourceQueryParameters queryParameters);

        Task<Customer> GetByIdAsync(string id, string navigationPropertiesToInclude = null);
    }
}
