﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface IOrderDetailsRepository : IRepository<OrderDetail>
    {
        Task<bool> OrderDetailsExistsAsync(int orderId, int productId);

        Task<OrderDetail> GetByIdAsync(int orderId, int productId);
    }
}
