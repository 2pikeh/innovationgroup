﻿using System.Threading.Tasks;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface IRepository<in TEntity> where TEntity : class
    {
        Task<int> CreateAsync(TEntity entity);

        Task<int> Update(TEntity entity);
    }
}
