﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Orders;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface IOrdersRepository : IRepository<Order>
    {
        PagedDataset<Order> GetAll(OrderQueryParameters queryParameters);

        Task<int> DeleteAsync(int id);

        Task<Order> GetByIdAsync(int id, bool loadOrderDetails = false);

        Task<bool> OrderExistsAsync(int id);

        Task<IEnumerable<Order>> GetCustomerOrders(string customerId, bool loadOrderDetails);

        Task<Order> GetCustomerOrderAsync(string customerId, int orderId, bool loadOrderDetails);

        IEnumerable<Order> GetEmployeeOrders(int employeeId);

        Task<Order> GetEmployeeOrderAsync(int employeeId, int orderId);

    }
}
