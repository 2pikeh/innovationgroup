﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Services;

namespace InnovationGroup.Data.Repositories.Interfaces
{
    public interface IEmployeesRepository : IRepository<Employee>
    {
        Task<Employee> GetByIdAsync(int id);

        Task<bool> EmployeeExistsAsync(int id);

        Task<int> DeleteAsync(int id);

        PagedDataset<Employee> GetAll(ResourceQueryParameters queryParameters);
    }
}
