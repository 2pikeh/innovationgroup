﻿using System.Linq;
using System.Threading.Tasks;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace InnovationGroup.Data.Repositories
{
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly IInnovationGroupContext _dbContext;

        protected RepositoryBase(IInnovationGroupContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll() => GetEntitySet().AsNoTracking();

        public async Task<int> CreateAsync(TEntity entity)
        {
            await GetEntitySet().AddAsync(entity);
            return await SaveChangesAsync();
        }

        public async Task<int> Update(TEntity entity)
        {
            GetEntitySet().Update(entity);
            return await SaveChangesAsync();
        }

        protected async Task<int> SaveChangesAsync() => await _dbContext.SaveChangesAsync();

        protected DbSet<TEntity> GetEntitySet() => _dbContext.Set<TEntity>();

        protected EntityEntry<T> Entry<T>(T entity) where T : class => _dbContext.Entry(entity);
    }
}
