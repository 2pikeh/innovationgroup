﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InnovationGroup.Data.Repositories
{
    public class OrderDetailsRepository : RepositoryBase<OrderDetail>, IOrderDetailsRepository
    {
        public OrderDetailsRepository(IInnovationGroupContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<OrderDetail> GetByIdAsync(int orderId, int productId) => 
            await GetEntitySet()
                .FirstOrDefaultAsync(SearchFilter(orderId, productId));

        public async Task<bool> OrderDetailsExistsAsync(int orderId, int productId) => 
            await GetEntitySet()
                .AnyAsync(SearchFilter(orderId, productId));

        private static Expression<Func<OrderDetail, bool>> SearchFilter(int orderId, int productId) =>
            c => c.OrderId == orderId && c.ProductId == productId;
    }
}
