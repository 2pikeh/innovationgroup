﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Services;
using Microsoft.EntityFrameworkCore;

namespace InnovationGroup.Data.Repositories
{
    public class CategoriesRepository : RepositoryBase<Category>, ICategoriesRepository
    {
        private readonly IPagedDatasetService _pagingService;
        public CategoriesRepository(IInnovationGroupContext dbContext, IPagedDatasetService pagingService) 
            : base(dbContext)
        {
            _pagingService = pagingService;
        }

        public async Task<Category> GetByIdAsync(int id) =>
            await GetEntitySet().FirstOrDefaultAsync(c => c.CategoryId == id);

        public async Task<bool> CategoryExistsAsync(int id) =>
            await GetEntitySet()
                    .AnyAsync(c => c.CategoryId == id);

        public async Task<int> DeleteAsync(int id)
        {
            Category entity = await GetByIdAsync(id);
            GetEntitySet().Remove(entity);

            return await SaveChangesAsync();
        }

        public PagedDataset<Category> GetAll(ResourceQueryParameters queryParameters) =>
            _pagingService.ApplyPaging(GetAll(), queryParameters.PageNumber, queryParameters.PageSize);
    }
}
