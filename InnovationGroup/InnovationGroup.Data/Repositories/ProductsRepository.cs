﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InnovationGroup.Data.Repositories
{
    public class ProductsRepository : RepositoryBase<Product>, IProductsRepository
    {
        public ProductsRepository(IInnovationGroupContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Product> GetByIdAsync(int id) =>
            await GetEntitySet().FirstOrDefaultAsync(c => c.ProductId == id);

        public async Task<bool> ProductExistsAsync(int id) => 
            await GetEntitySet()
                .AnyAsync(c => c.ProductId == id);

        public async Task<int> DeleteAsync(int id)
        {
            Product entity = await GetByIdAsync(id);
            GetEntitySet().Remove(entity);

            return await SaveChangesAsync();
        }
    }
}
