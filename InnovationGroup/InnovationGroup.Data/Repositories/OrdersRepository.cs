﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Orders;
using Microsoft.EntityFrameworkCore;

namespace InnovationGroup.Data.Repositories
{
    public class OrdersRepository : RepositoryBase<Order>, IOrdersRepository
    {
        private readonly IPagedDatasetService _pagingService;

        public OrdersRepository(IInnovationGroupContext dbContext, IPagedDatasetService pagingService) 
            : base(dbContext)
        {
            _pagingService = pagingService;
        }

        public async Task<IEnumerable<Order>> GetCustomerOrders(string customerId, bool loadOrderDetails)
        {
            var customerOrders = GetEntitySet().Where(o =>
                string.Equals(o.CustomerId, customerId, StringComparison.InvariantCultureIgnoreCase));

            if (!loadOrderDetails)
                return customerOrders;

            foreach (var order in customerOrders)
            {
                await Entry(order).Navigation(nameof(Order.OrderDetails)).LoadAsync();
            }

            return customerOrders;
        }

        public async Task<Order> GetCustomerOrderAsync(string customerId, int orderId, bool loadOrderDetails)
        {
            var orderForCustomer = await GetEntitySet().FirstOrDefaultAsync(o =>
                string.Equals(o.CustomerId, customerId, StringComparison.InvariantCultureIgnoreCase) &&
                o.OrderId == orderId);

            if (loadOrderDetails)
            {
                await Entry(orderForCustomer).Navigation(nameof(Order.OrderDetails)).LoadAsync();
            }

            return orderForCustomer;
        }

        public IEnumerable<Order> GetEmployeeOrders(int employeeId) =>
            GetAll().Where(o => o.EmployeeId == employeeId);

        public async Task<Order> GetEmployeeOrderAsync(int employeeId, int orderId) =>
            await GetAll().FirstOrDefaultAsync(o => o.EmployeeId == employeeId && o.OrderId == orderId);

        public async Task<Order> GetByIdAsync(int id, bool loadOrderDetails = false)
        {
            var order = await GetEntitySet()
                .FirstOrDefaultAsync(o => o.OrderId == id);

            if (loadOrderDetails)
            {
                await Entry(order).Navigation(nameof(Order.OrderDetails)).LoadAsync();
            }

            return order;
        }

        public async Task<bool> OrderExistsAsync(int id) => 
            await GetEntitySet()
                .AnyAsync(c => c.OrderId == id);

        public PagedDataset<Order> GetAll(OrderQueryParameters queryParameters) =>
            _pagingService.ApplyPaging(GetAll(), queryParameters.PageNumber, queryParameters.PageSize);

        public async Task<int> DeleteAsync(int id)
        {
            Order entity = await GetByIdAsync(id);
            GetEntitySet().Remove(entity);

            return await SaveChangesAsync();
        }
    }
}
