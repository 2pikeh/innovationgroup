﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Services;
using Microsoft.EntityFrameworkCore;

namespace InnovationGroup.Data.Repositories
{
    public class EmployeesRepository : RepositoryBase<Employee>, IEmployeesRepository
    {
        private readonly IPagedDatasetService _pagingService;

        public EmployeesRepository(IInnovationGroupContext dbContext, IPagedDatasetService pagingService) 
            : base(dbContext)
        {
            _pagingService = pagingService;
        }

        public async Task<Employee> GetByIdAsync(int id) =>
            await GetEntitySet()
                .FirstOrDefaultAsync(c => c.EmployeeId == id);

        public async Task<bool> EmployeeExistsAsync(int id) =>
            await GetEntitySet()
                .AnyAsync(c => c.EmployeeId == id);

        public async Task<int> DeleteAsync(int id)
        {
            Employee entity = await GetByIdAsync(id);
            GetEntitySet().Remove(entity);

            return await SaveChangesAsync();
        }

        public PagedDataset<Employee> GetAll(ResourceQueryParameters queryParameters) =>
            _pagingService.ApplyPaging(GetAll(), queryParameters.PageNumber, queryParameters.PageSize);
    }
}
