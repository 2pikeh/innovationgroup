﻿using System;
using System.Collections.Generic;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Domain.Categories;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Orders;

namespace InnovationGroup.Data.Services
{
    public static class PropertyMappingTables
    {
        public static readonly Dictionary<string, PropertyMappingValue> CategoriesPropertyMapping =
   new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
   {
               { nameof(CategoriesDto.Id), new PropertyMappingValue(new List<string> { nameof(Category.CategoryId) } ) },
               { nameof(CategoriesDto.Name), new PropertyMappingValue(new List<string> { nameof(Category.CategoryName) } )},
               { nameof(CategoriesDto.Description), new PropertyMappingValue(new List<string> { nameof(Category.Description) } , true) },
               { nameof(CategoriesDto.Picture), new PropertyMappingValue(new List<string> { nameof(Category.Picture) }) },
               { nameof(CategoriesDto.Products), new PropertyMappingValue(new List<string> { nameof(Category.Products) }, true) }
   };

        public static readonly Dictionary<string, PropertyMappingValue> CustomersPropertyMapping =
            new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
            {
                { nameof(CustomersDto.Id), new PropertyMappingValue(new List<string> { nameof(Customer.CustomerId) } ) },
                { nameof(CustomersDto.Company), new PropertyMappingValue(new List<string> { nameof(Customer.CompanyName) } )},
                { nameof(CustomersDto.ContactName), new PropertyMappingValue(new List<string> { nameof(Customer.ContactName) }) },
                { nameof(CustomersDto.ContactTitle), new PropertyMappingValue(new List<string> { nameof(Customer.ContactTitle) }) },
                { nameof(CustomersDto.Address), new PropertyMappingValue(new List<string> { nameof(Customer.Address) }) },
                { nameof(CustomersDto.City), new PropertyMappingValue(new List<string> { nameof(Customer.City) }) },
                { nameof(CustomersDto.County), new PropertyMappingValue(new List<string> { nameof(Customer.Country) }) },
                { nameof(CustomersDto.PostCode), new PropertyMappingValue(new List<string> { nameof(Customer.PostCode) }) },
                { nameof(CustomersDto.Country), new PropertyMappingValue(new List<string> { nameof(Customer.Country) }) },
                { nameof(CustomersDto.Fax), new PropertyMappingValue(new List<string> { nameof(Customer.Fax) }) },
                { nameof(CustomersDto.Phone), new PropertyMappingValue(new List<string> { nameof(Customer.Phone) }) },
                { nameof(CustomersDto.Orders), new PropertyMappingValue(new List<string> { nameof(Customer.Orders) }) }
            };

        public static readonly Dictionary<string, PropertyMappingValue> EmployeePropertyMapping =
            new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
            {
                { nameof(EmployeesDto.Id), new PropertyMappingValue(new List<string> { nameof(Employee.EmployeeId) } ) },
                { nameof(EmployeesDto.Name), new PropertyMappingValue(new List<string> { nameof(Employee.FirstName), nameof(Employee.LastName) } )},
                { nameof(EmployeesDto.JobTitle), new PropertyMappingValue(new List<string> { nameof(Employee.JobTitle) }) },
                { nameof(EmployeesDto.TitleOfCourtesy), new PropertyMappingValue(new List<string> { nameof(Employee.TitleOfCourtesy) }) },
                { nameof(EmployeesDto.Address), new PropertyMappingValue(new List<string> { nameof(Employee.Address) }) },
                { nameof(EmployeesDto.City), new PropertyMappingValue(new List<string> { nameof(Employee.City) }) },
                { nameof(EmployeesDto.Region), new PropertyMappingValue(new List<string> { nameof(Employee.Region) }) },
                { nameof(EmployeesDto.PostCode), new PropertyMappingValue(new List<string> { nameof(Employee.PostCode) }) },
                { nameof(EmployeesDto.Country), new PropertyMappingValue(new List<string> { nameof(Employee.Country) }) },
                { nameof(EmployeesDto.HomePhone), new PropertyMappingValue(new List<string> { nameof(Employee.Country) }) },
                { nameof(EmployeesDto.Photo), new PropertyMappingValue(new List<string> { nameof(Employee.Photo) }) },
                { nameof(EmployeesDto.Notes), new PropertyMappingValue(new List<string> { nameof(Employee.Notes) }) },
                { nameof(EmployeesDto.DoB), new PropertyMappingValue(new List<string> { nameof(Employee.BirthDate) }) }
            };

        public static readonly Dictionary<string, PropertyMappingValue> OrdersPropertyMapping =
            new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
            {
                { nameof(OrdersDto.Id), new PropertyMappingValue(new List<string> { nameof(Order.OrderId) } ) },
                { nameof(OrdersDto.CustomerId), new PropertyMappingValue(new List<string> { nameof(Order.CustomerId) } )},
                { nameof(OrdersDto.OrderDate), new PropertyMappingValue(new List<string> { nameof(Order.OrderDate) }) },
                { nameof(OrdersDto.RequiredDate), new PropertyMappingValue(new List<string> { nameof(Order.RequiredDate) }) },
                { nameof(OrdersDto.ShippedDate), new PropertyMappingValue(new List<string> { nameof(Order.ShippedDate) }) },
                { nameof(OrdersDto.ShippingCost), new PropertyMappingValue(new List<string> { nameof(Order.ShippingCost) }) },
                { nameof(OrdersDto.ShipName), new PropertyMappingValue(new List<string> { nameof(Order.ShipName) }) },
                { nameof(OrdersDto.ShipAddress), new PropertyMappingValue(new List<string> { nameof(Order.ShipAddress) }) },
                { nameof(OrdersDto.ShipCity), new PropertyMappingValue(new List<string> { nameof(Order.ShipCity) }) },
                { nameof(OrdersDto.ShipRegion), new PropertyMappingValue(new List<string> { nameof(Order.ShipCountry) }) },
                { nameof(OrdersDto.ShipPostcode), new PropertyMappingValue(new List<string> { nameof(Order.ShipPostCode) }) },
                { nameof(OrdersDto.ShipCountry), new PropertyMappingValue(new List<string> { nameof(Order.ShipCountry) }) },
                { nameof(OrdersDto.Customer), new PropertyMappingValue(new List<string> { nameof(Order.Customer) }) },
                { nameof(OrdersDto.Employee), new PropertyMappingValue(new List<string> { nameof(Order.Employee) }) },
                { nameof(OrdersDto.OrderDetails), new PropertyMappingValue(new List<string> { nameof(Order.OrderDetails) }) }
            };
    }
}
