﻿using System.Collections.Generic;

namespace InnovationGroup.Data.Services
{
    public interface IPropertyMapping
    {
        Dictionary<string, PropertyMappingValue> MappingDictionary { get; }
    }
}
