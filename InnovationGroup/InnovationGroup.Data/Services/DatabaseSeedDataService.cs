﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.DataProvider;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace InnovationGroup.Data.Services
{
    public class DatabaseSeedDataService : IDatabaseSeedDataService
    {
        private readonly InnovationGroupContext _ctx;

        public DatabaseSeedDataService(InnovationGroupContext ctx)
        {
            _ctx = ctx;
        }

        public void Seed()
        {
            using (_ctx)
            {
                _ctx.Database.Migrate();
                var baseDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"SeedData");

                if (!_ctx.Categories.Any())
                {
                    var categories =
                        JsonConvert.DeserializeObject<IEnumerable<Category>>(
                            File.ReadAllText(Path.Combine(baseDir, "categories.json")));
                    _ctx.Categories.AddRange(categories);
                    _ctx.SaveChanges();
                }

                if (!_ctx.Customers.Any())
                {
                    var customers =
                        JsonConvert.DeserializeObject<IEnumerable<Customer>>(
                            File.ReadAllText(Path.Combine(baseDir, "customers.json")));
                    _ctx.Customers.AddRange(customers);
                    _ctx.SaveChanges();
                }

                if (!_ctx.Products.Any())
                {
                    var products =
                        JsonConvert.DeserializeObject<IEnumerable<Product>>(
                            File.ReadAllText(Path.Combine(baseDir, "products.json")));
                    _ctx.Products.AddRange(products);
                    _ctx.SaveChanges();
                }

                if (!_ctx.Employees.Any())
                {
                    var employees =
                        JsonConvert.DeserializeObject<IEnumerable<Employee>>(
                            File.ReadAllText(Path.Combine(baseDir, "employees.json")));
                    _ctx.Employees.AddRange(employees);
                    _ctx.SaveChanges();
                }

                if (!_ctx.Orders.Any())
                {
                    ExecuteWithIdentityInsertRemoval<Order>(_ctx, (inputCtx) =>
                    {
                        var orders =
                                JsonConvert.DeserializeObject<IEnumerable<Order>>(
                                    File.ReadAllText(Path.Combine(baseDir, "orders.json")));
                        _ctx.Orders.AddRange(orders);
                    });
                }

                if (_ctx.OrderDetails.Any())
                    return;

                var orderDetails =
                    JsonConvert.DeserializeObject<IEnumerable<OrderDetail>>(
                        File.ReadAllText(Path.Combine(baseDir, "order_details.json")));
                _ctx.OrderDetails.AddRange(orderDetails);
                _ctx.SaveChanges();
            }
        }

        private static void ExecuteWithIdentityInsertRemoval<TModel>(
            InnovationGroupContext context,
            Action<InnovationGroupContext> action) where TModel : class
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT " + $"{typeof(TModel).Name}s" + " ON;");
                    context.SaveChanges();

                    action(context);
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    context.Database.ExecuteSqlCommand($"SET IDENTITY_INSERT " + $"{typeof(TModel).Name}s" + " OFF;");
                    context.SaveChanges();
                }
            }
        }
    }
}
