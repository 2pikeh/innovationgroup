﻿using System.Linq;
using InnovationGroup.Domain.Services;

namespace InnovationGroup.Data.Services
{
    public interface IPagedDatasetService
    {
        PagedDataset<T> ApplyPaging<T>(IQueryable<T> source, int pageNumber, int pageSize);

        PagingMetadataWithNavigationUris GetPagingMetadata<T>(PagedDataset<T> source,
            ResourceQueryParameters queryParameters, string routeName);
    }
}
