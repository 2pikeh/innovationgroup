﻿using System;
using System.Collections.Generic;
using System.Linq;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Domain.Categories;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Orders;

namespace InnovationGroup.Data.Services
{
    public class PropertyMappingService : IPropertyMappingService
    {
        private readonly IList<IPropertyMapping> _propertyMappings = new List<IPropertyMapping>();

        public PropertyMappingService()
        {
            _propertyMappings.Add(new PropertyMapping<CategoriesDto, Category>(PropertyMappingTables.CategoriesPropertyMapping));
            _propertyMappings.Add(new PropertyMapping<CustomersDto, Customer>(PropertyMappingTables.CustomersPropertyMapping));
            _propertyMappings.Add(new PropertyMapping<EmployeesDto, Employee>(PropertyMappingTables.EmployeePropertyMapping));
            _propertyMappings.Add(new PropertyMapping<OrdersDto, Order>(PropertyMappingTables.OrdersPropertyMapping));
        }

        public Dictionary<string, PropertyMappingValue>  GetPropertyMapping
            <TSource, TDestination>()
        {
            // get matching mapping
            var matchingMapping = _propertyMappings.OfType<PropertyMapping<TSource, TDestination>>();
            var propertyMappings = matchingMapping as PropertyMapping<TSource, TDestination>[] ?? matchingMapping.ToArray();

            if (propertyMappings.Any())
            {
                return propertyMappings.First().MappingDictionary;
            }

            throw new Exception($"Cannot find exact property mapping instance for <{typeof(TSource)},{typeof(TDestination)}");
        }

        public bool ValidMappingExistsFor<TSource, TDestination>(string fields)
        {
            var propertyMapping = GetPropertyMapping<TSource, TDestination>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                return true;
            }

            // the string is separated by ",", so we split it.
            var fieldsAfterSplit = fields.Split(',');

            // run through the fields clauses
            return (fieldsAfterSplit.Select(field => field.Trim())
                    .Select(trimmedField => new {trimmedField, indexOfFirstSpace = trimmedField.IndexOf(" ")})
                    .Select(@t => @t.indexOfFirstSpace == -1 ? @t.trimmedField : @t.trimmedField.Remove(@t.indexOfFirstSpace)))
                .All(propertyName => propertyMapping.ContainsKey(propertyName));
        }
    }
}
