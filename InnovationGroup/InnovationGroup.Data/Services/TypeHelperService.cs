﻿using System;
using System.Linq;
using System.Reflection;

namespace InnovationGroup.Data.Services
{
    public class TypeHelperService : ITypeHelperService
    {
        public bool TypeHasProperties(Type type, string fields)
        {
            if (string.IsNullOrWhiteSpace(fields))
            {
                return true;
            }

            // the field are separated by ",", so we split it.
            var fieldsAfterSplit = fields.Split(',');

            // check if the requested fields exist on source - returns true if all checks out ok
            return fieldsAfterSplit.Select(field => field.Trim())
                .Select(propertyName => type.GetProperty(
                    propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance))
                .All(propertyInfo => propertyInfo != null);
        }

        public bool TypeHasProperties<T>(string fields) => TypeHasProperties(typeof(T), fields);
    }
}
