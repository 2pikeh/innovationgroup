﻿using System.Linq;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;

namespace InnovationGroup.Data.Services
{
    public class PagedDatasetService : IPagedDatasetService
    {
        private readonly IResourceUriService _resourceUriService;

        public PagedDatasetService(IResourceUriService resourceUriService)
        {
            _resourceUriService = resourceUriService;
        }

        public PagedDataset<T> ApplyPaging<T>(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var totalCount = source.Count();
            var pageItems = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PagedDataset<T>(pageItems, totalCount, pageNumber, pageSize);
        }

        public PagingMetadataWithNavigationUris GetPagingMetadata<T>(PagedDataset<T> source,
            ResourceQueryParameters queryParameters, string routeName)
        {
            var previousPageUri = source.HasPreviousPage
                ? _resourceUriService.CreateResourceUri(queryParameters, ResourceUriType.PreviousPage,
                    routeName)
                : string.Empty;

            var nextPageUri = source.HasNextPage
                ? _resourceUriService.CreateResourceUri(queryParameters, ResourceUriType.NextPage,
                    routeName)
                : string.Empty;

            return new PagingMetadataWithNavigationUris(
                source.PageSize,
                source.CurrentPage,
                source.TotalPages,
                source.TotalCount,
                previousPageUri,
                nextPageUri
            );
        }
    }
}
