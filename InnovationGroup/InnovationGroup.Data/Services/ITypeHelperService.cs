﻿using System;

namespace InnovationGroup.Data.Services
{
    public interface ITypeHelperService
    {
        bool TypeHasProperties<T>(string fields);
        bool TypeHasProperties(Type type, string fields);
    }
}
