﻿namespace InnovationGroup.Data.Services
{
    public interface IDatabaseSeedDataService
    {
        void Seed();
    }
}
