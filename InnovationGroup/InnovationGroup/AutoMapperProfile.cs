﻿using AutoMapper;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Domain.Categories;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.OrderDetails;
using InnovationGroup.Domain.Orders;
using InnovationGroup.Domain.Products;

namespace InnovationGroup
{
    /// <summary>
    /// Auto Mapper profile for type mappings.
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Bootstraps auto-mapper for type mapping transformation. Used by the API and
        /// integration tests project
        /// </summary>
        public AutoMapperProfile()
        {
            //Category
            CreateMap<Category, CategoriesDto>()
                .ForMember(target => target.Id, opt => opt.MapFrom(src => src.CategoryId))
                .ForMember(target => target.Name, opt => opt.MapFrom(src => src.CategoryName));

            CreateMap<CategoriesCreateDto, Category>()
                .ForMember(target => target.CategoryName, opt => opt.MapFrom(src => src.Name));

            CreateMap<CategoriesUpdateDto, Category>()
                .ForMember(target => target.CategoryName, opt => opt.MapFrom(src => src.Name));

            CreateMap<Category, CategoriesUpdateDto>()
                .ForMember(target => target.Name, opt => opt.MapFrom(src => src.CategoryName));

            //Customer
            CreateMap<Customer, CustomersDto>()
                .ForMember(target => target.Id, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(target => target.Company, opt => opt.MapFrom(src => src.CompanyName));
            CreateMap<Customer, CustomersUpdateDto>()
                .ForMember(target => target.Company, opt => opt.MapFrom(src => src.CompanyName));
            CreateMap<CustomersCreateDto, Customer>()
                .ForMember(target => target.CustomerId, opt => opt.MapFrom(src => src.Id))
                .ForMember(target => target.CompanyName, opt => opt.MapFrom(src => src.Company));
            CreateMap<CustomersUpdateDto, Customer>()
                .ForMember(target => target.CompanyName, opt => opt.MapFrom(src => src.Company));

            //Employee
            CreateMap<Employee, EmployeesDto>()
                .ForMember(target => target.Id, opt => opt.MapFrom(src => src.EmployeeId))
                .ForMember(target => target.Name, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(target => target.DoB, opt => opt.MapFrom(src => src.BirthDate))
                .ForMember(target => target.OfficeExt, opt => opt.MapFrom(src => src.Extension));

            CreateMap<Employee, EmployeesUpdateDto>()
                .ForMember(target => target.Title, opt => opt.MapFrom(src => src.JobTitle))
                .ForMember(target => target.PostalCode, opt => opt.MapFrom(src => $"{src.PostCode}"));

            CreateMap<EmployeesCreateDto, Employee>();
            CreateMap<EmployeesUpdateDto, Employee>()
                .ForMember(target => target.JobTitle, opt => opt.MapFrom(src => src.Title))
                .ForMember(target => target.PostCode, opt => opt.MapFrom(src => src.PostalCode));

            //Order
            CreateMap<Order, OrdersDto>()
                .ForMember(target => target.Id, opt => opt.MapFrom(src => src.OrderId))
                .ForMember(target => target.ShippingCost, opt => opt.MapFrom(src => src.ShippingCost));

            CreateMap<OrdersCreateDto, Order>()
                .ForMember(target => target.ShipCounty, opt => opt.MapFrom(src => src.ShipRegion));
            //.ForMember(target => target., opt => opt.MapFrom(src => src.ShippingCost));

            //OrderDetail
            CreateMap<OrderDetail, OrderDetailsDto>();
            CreateMap<OrderDetailsCreateDto, OrderDetail>();
            CreateMap<OrderDetailsUpdateDto, OrderDetail>();

            //Product
            CreateMap<Product, ProductsDtoBase>()
                .ForMember(target => target.Name, opt => opt.MapFrom(src => src.ProductName));
            CreateMap<ProductsCreateDto, Product>()
                .ForMember(target => target.ProductName, opt => opt.MapFrom(src => src.Name));
            CreateMap<ProductsUpdateDto, Product>()
                .ForMember(target => target.ProductName, opt => opt.MapFrom(src => src.Name));
        }
    }
}
