﻿using System.Collections.Generic;
using System.Linq;
using AspNetCoreRateLimit;
using AutoMapper;
using FluentValidation;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Repositories;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Categories;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Orders;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Services.Validators.Categories;
using InnovationGroup.Domain.Services.Validators.Customers;
using InnovationGroup.Domain.Services.Validators.Employees;
using InnovationGroup.Domain.Services.Validators.Orders;
using InnovationGroup.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace InnovationGroup
{
    /// <summary>
    /// Helper class for registering services with the container.
    /// </summary>
    public static class StartupHelper
    {
        /// <summary>
        /// Bootstraps auto-mapper
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterAutoMapper(IServiceCollection services)
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfile>());
            var mapper = mapperConfiguration.CreateMapper();
            services.AddSingleton(mapper);
        }

        /// <summary>
        /// Register repository services
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterRepositoryTypes(IServiceCollection services)
        {
            services.AddScoped<IInnovationGroupContext, InnovationGroupContext>();
            services.AddScoped<ICategoriesRepository, CategoriesRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();

            services.AddScoped<IEmployeesRepository, EmployeesRepository>();
            services.AddScoped<IOrderDetailsRepository, OrderDetailsRepository>();
            services.AddScoped<IOrdersRepository, OrdersRepository>();

            services.AddScoped<IProductsRepository, ProductsRepository>();
            services.AddScoped<IPagedDatasetService, PagedDatasetService>();

            services.AddScoped<IPagedDatasetService, PagedDatasetService>();

            if (services.All(x => x.ServiceType != typeof(IDatabaseSeedDataService)))
            {
                // Service doesn't exist, do something
                services.AddScoped<IDatabaseSeedDataService, DatabaseSeedDataService>();
            }
        }

        /// <summary>
        /// Register utility services
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterHelperServices(IServiceCollection services)
        {
            services.AddScoped<ITypeHelperService, TypeHelperService>();
            services.AddScoped<IPropertyMappingService, PropertyMappingService>();
            services.AddScoped<IResourceUriService, ResourceUriService>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped(provider =>
            {
                var actionContext = provider.GetRequiredService<IActionContextAccessor>().ActionContext;
                var factory = provider.GetRequiredService<IUrlHelperFactory>();

                return factory.GetUrlHelper(actionContext);
            });
        }

        /// <summary>
        /// Register request throttling services with the container
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterRequestRateLimitingServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(options =>
            {
                options.GeneralRules = new List<RateLimitRule>
                {
                    new RateLimitRule
                    {
                        Endpoint = "*",
                        Limit = 5,
                        Period = "10s"
                    }
                };
            });

            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
        }

        /// <summary>
        /// Register validators with the container
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterValidationServices(IServiceCollection services)
        {
            // Register framework validation filters
            services.AddScoped<OrdersCollectionValidationFilter>();
            services.AddScoped<CustomersValidationFilter>();
            services.AddScoped<EmployeesValidationFilter>();
            services.AddScoped<CategoriesValidationFilter>();

            // Register FluentValidation validators
            services.AddTransient<IValidator<CategoriesCreateDto>, CategoriesCreateDtoValidator>();
            services.AddTransient<IValidator<CategoriesUpdateDto>, CategoriesUpdateDtoValidator>();

            services.AddTransient<IValidator<CustomersCreateDto>, CustomerCreateDtoValidator>();
            services.AddTransient<IValidator<CustomersUpdateDto>, CustomersUpdateDtoValidator>();

            services.AddTransient<IValidator<OrdersCreateDto>, OrdersCreateDtoValidator>();
            services.AddTransient<IValidator<OrdersUpdateDto>, OrdersUpdateDtoValidator>();
            services.AddTransient<IValidator<EmployeesCreateDto>, EmployeeCreateDtoValidator>();
        }
    }
}
