﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using InnovationGroup.Data;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Orders;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;
using InnovationGroup.Filters;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using CollectionResource = InnovationGroup.Domain.Uri.CollectionResource;

namespace InnovationGroup.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Endpoints for manipulating an order resource
    /// </summary>
    [ApiController]
    [Route("api/Customer/{customerId}/Order")]
    public class OrdersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IOrdersRepository _orderRepository;
        private readonly IResourceUriService _resourceUriService;

        private readonly ICustomerRepository _customerRepository;
        private readonly IPagedDatasetService _pagingService;

        /// <summary>
        /// Default injection constructor 
        /// </summary>
        /// <param name="mapper">Auto-mapper type mapping service</param>
        /// <param name="orderRepository"></param>
        /// <param name="resourceUriService"></param>
        /// <param name="customerRepository"></param>
        /// <param name="pagingService"></param>
        public OrdersController(IMapper mapper,
            IOrdersRepository orderRepository, 
            IResourceUriService resourceUriService,
            ICustomerRepository customerRepository,
            IPagedDatasetService pagingService)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
            _resourceUriService = resourceUriService;

            _customerRepository = customerRepository;
            _pagingService = pagingService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediaType"></param>
        /// <param name="queryParameters"></param>
        /// <param name="customerId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Customer/VINET/Order/10248
        ///     GET api/Customer/VINET/Order/10248?loadOrderDetails=true?pageNumber=1&pageSize=3
        ///     GET api/Customer/VINET/Order/10248?pageNumber=1&pageSize=3&propertiesToInclude=id,orderDate,shippedDate
        ///     GET api/Customer/VINET/Order/10248?loadOrderDetails=true
        ///
        /// </remarks>
        [ServiceFilter(typeof(OrdersCollectionValidationFilter))]
        [HttpGet("{orderId}", Name = StringConstants.GetOrderForCustomer)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetOrderForCustomer([FromHeader(Name = "Accept")] string mediaType,
            [FromQuery] OrderQueryParameters queryParameters,
            string customerId, int orderId)
        {
            if (!await _customerRepository.CustomerExistsAsync(customerId))
            {
                return NotFound();
            }

            Order order =
                await _orderRepository.GetCustomerOrderAsync(customerId, orderId, queryParameters.LoadOrderDetails);

            if (order == null)
            {
                return NotFound();
            }

            var mappedOrder = _mapper.Map<OrdersDto>(order);
            var filteredOrder =
                mappedOrder.ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude) as
                    IDictionary<string, object>;

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                return Ok(filteredOrder);
            }

            IEnumerable<RouteUriMetadata> supportedRoutes =
                _resourceUriService.GenerateOrderRoutes(mappedOrder.Id, mappedOrder.CustomerId, ResetPagingToSingleItemDefault(queryParameters));

            IEnumerable<UriDto> supportedRoutesAsUris =
                _resourceUriService.CreateResourceUri(mappedOrder.Id.ToString(), supportedRoutes);

            filteredOrder.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUris);

            return Ok(filteredOrder);
        }

        /// <summary>
        /// Get all orders for a specific customer resource
        /// </summary>
        /// <param name="mediaType"></param>
        /// <param name="queryParameters"></param>
        /// <param name="customerId"></param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Customer/VINET/Order?loadOrderDetails=true
        ///     GET api/Customer/VINET/Order/10248?pageNumber=1&pageSize=3&propertiesToInclude=id,orderDate,shippedDate
        ///
        /// </remarks>
        /// <returns></returns>
        [ServiceFilter(typeof(OrdersCollectionValidationFilter))]
        [HttpGet(Name = StringConstants.GetAllOrdersForCustomer)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAllOrdersForCustomer([FromHeader(Name = "Accept")] string mediaType,
            [FromQuery] OrderQueryParameters queryParameters,
            string customerId)
        {
            if (!await _customerRepository.CustomerExistsAsync(customerId))
            {
                return NotFound();
            }

            IEnumerable<Order> orders =
                await _orderRepository.GetCustomerOrders(customerId, queryParameters.LoadOrderDetails);

            if (orders == null)
            {
                return NotFound();
            }

            var mappedOrder = _mapper.Map<IEnumerable<OrdersDto>>(orders);
            IEnumerable<ExpandoObject> filteredOrders =
                mappedOrder.ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude);

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                return Ok(new CollectionResource(filteredOrders.Select(c => c as IDictionary<string, object>)));
            }

            IEnumerable<UriDto> collectionUri = _resourceUriService.CreateResourceUri(queryParameters, false, false,
                StringConstants.GetAllOrdersForCustomer);

            var ordersWithUri = filteredOrders.Select(o =>
            {
                var order = o as IDictionary<string, object>;
                var orderId = (int)order[nameof(OrdersDto.Id)];

                IEnumerable<RouteUriMetadata> supportedRoutes =
                    _resourceUriService.GenerateOrderRoutes(orderId, customerId, queryParameters);

                IEnumerable<UriDto> supportedRoutesAsUri =
                    _resourceUriService.CreateResourceUri(orderId.ToString(), supportedRoutes);

                order.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUri);

                return order;
            });

            return Ok(new CollectionResourceWithUri(ordersWithUri, collectionUri));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="createRequest"></param>
        /// <returns></returns>
        /// <remarks>    
        /// POST api/Customer/{customerId}/Order/
        /// {
        ///   "customerId": "VINET",
        ///    "orderDate": "",
        ///    "shippedDate": "",
        ///    "shipName": "",
        ///    "shipAddress": "",
        ///    "postCode": 123456
        /// }
        /// 
        /// </remarks>
        [HttpPost(Name = StringConstants.AddNewOrderForCustomer)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(422)]
        public async Task<IActionResult> AddNewOrderForCustomer([FromBody] OrdersCreateDto createRequest)
        {
            if (createRequest == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if (!await _customerRepository.CustomerExistsAsync(createRequest.CustomerId))
            {
                return NotFound(createRequest.CustomerId);
            }

            var order = _mapper.Map<Order>(createRequest);
            await _orderRepository.CreateAsync(order);

            var createdOrder = _mapper.Map<OrdersDto>(order);

            return CreatedAtRoute(StringConstants.GetOrderForCustomer,
                new {customerId = createRequest.CustomerId, orderId = order.OrderId}, createdOrder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patchDocument"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        ///
        /// PATCH api/Orders/11
        /// [
        ///     {
        ///         "value": "",
        ///         "path": "/orderDate",
        ///         "op": "replace",
        ///         "from": "string"
        ///     }
        /// ]
        /// </remarks>
        [HttpPatch("{id}", Name = StringConstants.ExecutePartialUpdateOnOrder)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecutePartialUpdateOnOrder(int id,
            [FromBody] JsonPatchDocument<OrdersUpdateDto> patchDocument)
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            if (!await _orderRepository.OrderExistsAsync(id))
            {
                return NotFound();
            }

            var orderFromRepo = await _orderRepository.GetByIdAsync(id);
            var orderToPatch = _mapper.Map<OrdersUpdateDto>(orderFromRepo);
            patchDocument.ApplyTo(orderToPatch);

            _mapper.Map(orderToPatch, orderFromRepo);
            await _orderRepository.Update(orderFromRepo);

            return NoContent();
        }

        /// <summary>
        /// Deletes an order resource
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        /// 
        ///     DELETE: api/Orders/5
        ///
        /// </remarks>
        [HttpDelete("{id}", Name = StringConstants.DeleteOrder)]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            if (!await _orderRepository.OrderExistsAsync(id))
            {
                return NotFound();
            }

            await _orderRepository.DeleteAsync(id);

            return NoContent();
        }

        private OrderQueryParameters ResetPagingToSingleItemDefault(OrderQueryParameters queryParameters)
        {
            queryParameters.PageNumber = 1;
            queryParameters.PageSize = 1;

            return queryParameters;
        }
    }
}
