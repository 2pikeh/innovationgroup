﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using InnovationGroup.Data;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Categories;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;
using InnovationGroup.Filters;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace InnovationGroup.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Exposes endpoints for manipulating Category data.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICategoriesRepository _repository;
        private readonly IResourceUriService _resourceUriService;
        private readonly IPagedDatasetService _pagingService;
        private readonly ILogger<CategoriesController> _logger;

        /// <summary>
        /// Injection constructor
        /// </summary>
        /// <param name="mapper">Auto-mapper type mapping service</param>
        /// <param name="repository">Exposes CRUD operations for manipulating the category resource</param>
        /// <param name="resourceUriService">Generates URIs for the supported operation on a resource</param>
        /// <param name="pagingService"></param>
        /// <param name="logger"></param>
        public CategoriesController(IMapper mapper,
            ICategoriesRepository repository, 
            IResourceUriService resourceUriService,
            IPagedDatasetService pagingService,
            ILogger<CategoriesController> logger)
        {
            _mapper = mapper;
            _repository = repository;
            _resourceUriService = resourceUriService;
            _pagingService = pagingService;
            _logger = logger;
        }

        /// <summary>
        /// Gets all categories in the database based on the <paramref name="queryParameters"/> provided.
        /// If none is provided, the default is used. 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Categories
        ///     GET api/Categories? PageNumber = 1&PageSize = 3
        ///     GET api/Categories?PageNumber=1&PageSize=3&PropertiesToInclude=id,name
        /// </remarks>
        /// <param name="queryParameters">paging configuration</param>
        /// <param name="mediaType">the 'ACCEPT' media type - allows a customer HATEOAS media type for be requested</param>
        /// <returns></returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>
       // [HttpHead]
        [HttpGet(Name = nameof(StringConstants.GetAllCategories))]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetAllCategories([FromQuery] ResourceQueryParameters queryParameters,
            [FromHeader(Name = "Accept")] string mediaType)
        {
            PagedDataset<Category> pagedCategory = _repository.GetAll(queryParameters);

            if (pagedCategory.TotalCount == 0)
            {
                _logger.LogWarning("No category resource found in the repository.");
                return NotFound();
            }

            var pagingMetadata =
                _pagingService.GetPagingMetadata(pagedCategory, queryParameters, StringConstants.GetAllCategories);

            IEnumerable<ExpandoObject> filteredCategories =
                _mapper.Map<IEnumerable<CategoriesDto>>(pagedCategory)
                    .ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude);

            var xPagination = JsonConvert.SerializeObject(pagingMetadata);

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                Response?.Headers?.Add("X-Pagination", xPagination);

                var responseData =
                    new CollectionResource(filteredCategories.Select(c => c as IDictionary<string, object>));

                _logger.LogInformation(
                    $"Generating response for media-type [{mediaType}]: {JsonConvert.SerializeObject(responseData)}");

                return Ok(responseData);
            }

            _logger.LogInformation($"Detected [{mediaType}] request, embedding navigation url.");

            xPagination = JsonConvert.SerializeObject(new PagingMetadata(pagingMetadata));
            _logger.LogInformation(xPagination);

            Response?.Headers?.Add("X-Pagination", xPagination);
            
            var collectionUri = _resourceUriService.CreateResourceUri(queryParameters, pagedCategory.HasPreviousPage,
                pagedCategory.HasNextPage, StringConstants.GetAllCategories);

            var categoriesWithResourceUris = filteredCategories.Select(c =>
            {
                var category = c as IDictionary<string, object>;
                var categoryId = (int)category[nameof(CategoriesDto.Id)];

                IEnumerable<RouteUriMetadata> supportedRoutes =
                    _resourceUriService.GenerateCategoriesRoutes(categoryId, queryParameters);

                IEnumerable<UriDto> supportedRoutesAsUris =
                    _resourceUriService.CreateResourceUri(categoryId.ToString(), supportedRoutes);

                category.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUris);

                return category;
            });

            return Ok(new CollectionResourceWithUri(categoriesWithResourceUris, collectionUri));
        }

        /// <summary>
        /// Returns a single category. However, the <paramref name="queryParameters"/>'s PropertiesToInclude
        /// property can be used to select which properties to return (i.e return category name only, instead of
        /// an complete Category object graph
        /// </summary>
        /// <param name="id">The category id to search for</param>
        /// <param name="queryParameters">Parameters that allow paging configuration to be set and the properties that
        /// should be returned in the response</param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        /// GET api/Categories/11?PageNumber=1&PageSize=3
        /// GET api/Categories/11?PageNumber=1&PageSize=3&PropertiesToInclude=name%2Cdescription
        ///
        /// </remarks>
        ///
        [ServiceFilter(typeof(CategoriesValidationFilter))]
        [HttpGet("{id}", Name = nameof(StringConstants.GetCategoryById))]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetCategoryById(int id,
            [FromQuery] ResourceQueryParameters queryParameters,
            [FromHeader(Name = "Accept")] string mediaType)
        {
            Category category = await _repository.GetByIdAsync(id);

            if (category == null)
            {
                _logger.LogWarning("No category found.");
                return NotFound();
            }

            var mappedCategory = _mapper.Map<CategoriesDto>(category);
            var filteredCategory =
                mappedCategory.ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude) as
                    IDictionary<string, object>;

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                _logger.LogInformation(
                    $"Writing response data for [{mediaType}] -> {JsonConvert.SerializeObject(filteredCategory)}");

                return Ok(filteredCategory);
            }

            IEnumerable<RouteUriMetadata> supportedRoutes =
                _resourceUriService.GenerateCategoriesRoutes(mappedCategory.Id, queryParameters);

            IEnumerable<UriDto> supportedRoutesAsUris =
                _resourceUriService.CreateResourceUri(mappedCategory.Id.ToString(), supportedRoutes);

            filteredCategory.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUris);

            _logger.LogInformation(
                $"Writing response data for [{mediaType}] with embedded resource urls -> " +
                $"{JsonConvert.SerializeObject(filteredCategory)}");

            return Ok(filteredCategory);
        }

        /// <summary>
        /// Enables the creation of a new Category
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <remarks>
        ///
        /// POST /api/Categories
        /// {
        ///   "name": "swagger",
        ///    "description": "swagger test",
        ///    "picture": ""
        /// }
        /// 
        /// </remarks>

        [HttpPost(Name=StringConstants.AddNewCategory)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        public async Task<IActionResult> AddNewCategory([FromBody] CategoriesCreateDto request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            var mapToDbType = _mapper.Map<Category>(request);
            await _repository.CreateAsync(mapToDbType);
            var mapToDtoType = _mapper.Map<CategoriesDto>(mapToDbType);

            return CreatedAtRoute(
                nameof(StringConstants.GetCategoryById), 
                new {id = mapToDbType.CategoryId}, mapToDtoType);
        }

        /// <summary>
        /// Enables a 'deep' update of the category resource.
        /// Any property not sent, will be saved to it's default value.
        /// </summary>
        /// <param name="id">Id of category to update</param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        /// 
        /// PUT /api/Categories/5
        /// {
        ///   "name": "swagger",
        ///    "description": "swagger test",
        ///    "picture": ""
        /// }
        /// 
        /// </remarks>
        [HttpPut("{id}", Name=nameof(StringConstants.ExecuteDeepUpdateOnCategory))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecuteDeepUpdate(int id, [FromBody] CategoriesUpdateDto request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (!await _repository.CategoryExistsAsync(id))
            {
                _logger.LogInformation($"No category with id:{id} found in the repository.");
                return NotFound();
            }

            Category categoryFromRepo = await _repository.GetByIdAsync(id);
            _mapper.Map(request, categoryFromRepo);
            await _repository.Update(categoryFromRepo);

            return NoContent();
        }

        /// <summary>
        /// Performs a partial update to the category update. Only modified properties will be updated.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patchDocument">patch document containing the modified properties</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        ///
        /// PATCH api/Categories/11
        /// [
        ///     {
        ///         "value": "category-one",
        ///         "path": "/name",
        ///         "op": "replace",
        ///         "from": "string"
        ///     }
        /// ]
        /// </remarks>
        [HttpPatch("{id}", Name=nameof(StringConstants.ExecutePartialUpdateOnCategory))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecutePartialUpdate(int id,
            [FromBody] JsonPatchDocument<CategoriesUpdateDto> patchDocument)
        {
            if (patchDocument == null)
            {
                _logger.LogInformation("No patch document found.");
                return BadRequest();
            }

            if (!await _repository.CategoryExistsAsync(id))
            {
                _logger.LogInformation($"No category with id:{id} found in the repository.");
                return NotFound();
            }

            Category categoryFromRepo = await _repository.GetByIdAsync(id);
            var categoryPatch = _mapper.Map<CategoriesUpdateDto>(categoryFromRepo);
            patchDocument.ApplyTo(categoryPatch);
            _logger.LogInformation($"Patched model: {JsonConvert.SerializeObject(categoryPatch)}");

            _mapper.Map(categoryPatch, categoryFromRepo);
            await _repository.Update(categoryFromRepo);

            return NoContent();
        }

        /// <summary>
        /// Deletes a category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        /// 
        ///     DELETE: api/Category/5
        ///
        /// </remarks>
        [HttpDelete("{id}", Name=StringConstants.DeleteCategory)]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            if (!await _repository.CategoryExistsAsync(id))
            {
                return NotFound();
            }

            await _repository.DeleteAsync(id);

            return NoContent();
        }
    }
}
