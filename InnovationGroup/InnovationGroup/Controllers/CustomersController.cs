﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using InnovationGroup.Data;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;
using InnovationGroup.Filters;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace InnovationGroup.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Exposes endpoints for manipulating a customer resource.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _repository;
        private readonly IPagedDatasetService _pagingService;
        private readonly IResourceUriService _resourceUriService;

        /// <summary>
        /// Default injection constructor
        /// </summary>
        /// <param name="mapper">Auto-mapper type mapping service</param>
        /// <param name="repository">Exposes CRUD operations for manipulating the customer resource</param>
        /// <param name="resourceUriService">Generates URIs for the supported operation on a resource</param>
        /// <param name="pagingService">
        ///     Generates a paging filter that's applied to database
        ///     query before resource collection is returned.
        /// </param>
        public CustomersController(IMapper mapper,
            ICustomerRepository repository, 
            IPagedDatasetService pagingService,
            IResourceUriService resourceUriService)
        {
            _mapper = mapper;
            _repository = repository;
            _pagingService = pagingService;
            _resourceUriService = resourceUriService;
        }

        /// <summary>
        /// Gets the customer resource collection based on the
        /// <paramref name="queryParameters"/> provided.
        ///
        /// If none is provided, the default is used. 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Customers
        ///     GET api/Customers?pageNumber=1&pageSize=3
        ///     GET api/Customers?pageNumber=1&pageSize=3&propertiesToInclude=id,company,contactTitle,contactName
        /// </remarks>
        /// <param name="queryParameters">
        ///     Query parameters that can be used in shaping the
        ///     resource's response data. Parameters include pageSize,
        ///     pageNumber and resource fields to include.
        /// </param>
        /// <param name="mediaType">
        ///     the 'ACCEPT' media type - allows a customer HATEOAS media type for be requested
        /// </param>
        /// <returns></returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">Returned when the input parameters are invalid</response>
        [ServiceFilter(typeof(CustomersValidationFilter))]
       // [HttpHead]
        [HttpGet(Name = StringConstants.GetAllCustomers)]
        public IActionResult GetCustomers([FromQuery] CustomerResourceQueryParameters queryParameters,
            [FromHeader(Name = "Accept")] string mediaType)
        {
            PagedDataset<Customer> pagedCustomers = _repository.GetAll(queryParameters);
            if (pagedCustomers.TotalCount == 0)
            {
                return NotFound();
            }

            PagingMetadataWithNavigationUris pagingMetadata =
                _pagingService.GetPagingMetadata(pagedCustomers, queryParameters, StringConstants.GetAllCustomers);

            IEnumerable<ExpandoObject> filteredCustomers = 
                _mapper.Map<IEnumerable<CustomersDto>>(pagedCustomers)
                .ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude);

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                Response?.Headers?.Add("X-Pagination", JsonConvert.SerializeObject(pagingMetadata));

                return Ok(new CollectionResource(
                    filteredCustomers
                        .Select(c => c as IDictionary<string, object>)));
            }

            Response?.Headers?.Add("X-Pagination",
                JsonConvert.SerializeObject(new PagingMetadata(pagingMetadata)));

            var collectionUri = _resourceUriService.CreateResourceUri(queryParameters, pagedCustomers.HasPreviousPage,
                pagedCustomers.HasNextPage, StringConstants.GetCustomerById);

            var customersWithUri = filteredCustomers.Select(c =>
            {
                var customer = c as IDictionary<string, object>;
                var customerId = (string)customer[nameof(CustomersDto.Id)];

                IEnumerable<RouteUriMetadata> supportedRoutes =
                    _resourceUriService.GenerateCustomerRoutes(customerId, queryParameters);

                IEnumerable<UriDto> supportedRoutesAsUri =
                    _resourceUriService.CreateResourceUri(customerId.ToString(), supportedRoutes);

                customer.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUri);

                return customer;
            });

            return Ok(new CollectionResourceWithUri
                        (customersWithUri, collectionUri));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Customer id to search for.</param>
        /// <param name="queryParameters">
        ///     Parameters that allow paging configuration to be set and the properties that
        ///     should be returned in the response
        /// </param>
        /// <param name="mediaType">Adds support for custom hateoas media type</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Customers/11?pageNumber=1&pageSize=3
        ///     GET api/Customers/11?pageNumber=1&pageSize=3&propertiesToInclude=d,company,contactTitle
        ///
        /// </remarks>
        ///
        [ServiceFilter(typeof(CustomersValidationFilter))]
        [HttpGet("{id}", Name = StringConstants.GetCustomerById)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetCustomerById(string id, 
            [FromQuery] CustomerResourceQueryParameters queryParameters,
            [FromHeader(Name = "Accept")] string mediaType)
        {
            Customer customer = await _repository.GetByIdAsync(id, queryParameters.NavigationPropertiesToInclude);

            if (customer == null)
            {
                return NotFound();
            }

            var mappedCustomer = _mapper.Map<CustomersDto>(customer);
            var filteredCustomer =
                mappedCustomer.ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude) as
                    IDictionary<string, object>;

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                return Ok(filteredCustomer);
            }

            IEnumerable<RouteUriMetadata> supportedRoutes =
                _resourceUriService.GenerateCustomerRoutes(mappedCustomer.Id, queryParameters);

            IEnumerable<UriDto> supportedRoutesAsUris =
                _resourceUriService.CreateResourceUri(mappedCustomer.Id, supportedRoutes);

            filteredCustomer.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUris);

            return Ok(filteredCustomer);
        }

        /// <summary>
        /// Creates a new Customer resource
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <remarks>
        ///
        /// POST /api/Customers
        /// {
        ///   "company": "test company",
        ///    "contactName": "some one",
        ///    "contactTitle": "Mr",
        ///    "address": "1 Nowhere lane",
        ///    "city": "southampton",
        ///    "postCode": "ABC 123"
        ///    "phone": 123456
        /// }
        /// 
        /// </remarks>
        [HttpPost(Name = StringConstants.AddNewCustomer)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        public async Task<IActionResult> CreateNewCustomerEntry(
            [FromBody] CustomersCreateDto request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            var mapToRepoType = _mapper.Map<Customer>(request);
            await _repository.CreateAsync(mapToRepoType);
            var mapToDtoType = _mapper.Map<CustomersDto>(mapToRepoType);

            return CreatedAtRoute(nameof(GetCustomerById), 
                new { id = mapToRepoType.CustomerId }, mapToDtoType);
        }

        /// <summary>
        /// Performs a 'deep' update of the customer resource.
        /// Any property not sent, will be saved to it's default value.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        /// 
        /// PUT /api/Customers/5
        /// {
        ///   "company": "test company",
        ///    "contactName": "some one",
        ///    "contactTitle": "Mr",
        ///    "address": "1 Nowhere lane",
        ///    "city": "southampton",
        ///    "postCode": "ABC 123"
        ///    "phone": 123456
        /// }
        /// 
        /// </remarks>
        [HttpPut("{id}", Name = StringConstants.ExecuteDeepUpdateOnCustomer)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecuteDeepUpdate(string id, [FromBody] CustomersUpdateDto customer)
        {
            if (customer == null)
            {
                return BadRequest();
            }

            if (!await _repository.CustomerExistsAsync(id))
            {
                return NotFound();
            }

            Customer customerFromRepo = await _repository.GetByIdAsync(id);
            _mapper.Map(customer, customerFromRepo);
            await _repository.Update(customerFromRepo);

            return NoContent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patchDocument"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        ///
        /// PATCH api/Customers/11
        /// [
        ///     {
        ///         "value": "Company A",
        ///         "path": "/company",
        ///         "op": "replace",
        ///         "from": "string"
        ///     },
        ///     {
        ///         "value": "Some Person",
        ///         "path": "/contactName",
        ///         "op": "replace",
        ///         "from": "string"
        ///     }
        /// ]
        /// </remarks>
        [HttpPatch("{id}", Name = StringConstants.ExecutePartialUpdateOnCustomer)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecutePartialUpdate(string id,
            [FromBody] JsonPatchDocument<CustomersUpdateDto> patchDocument)
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            if (!await _repository.CustomerExistsAsync(id))
            {
                return NotFound();
            }

            var employeeFromRepo = await _repository.GetByIdAsync(id);
            var employeePatch = _mapper.Map<CustomersUpdateDto>(employeeFromRepo);
            patchDocument.ApplyTo(employeePatch);

            _mapper.Map(employeePatch, employeeFromRepo);
            await _repository.Update(employeeFromRepo);

            return NoContent();
        }

        /// <summary>
        /// Deletes a customer resource
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        /// 
        ///     DELETE: api/Customer/5
        ///
        /// </remarks>
        [HttpDelete("{id}", Name = StringConstants.DeleteCustomer)]
        public async Task<IActionResult> DeleteCustomer(string id)
        {
            if (!await _repository.CustomerExistsAsync(id))
            {
                return NotFound();
            }

            await _repository.DeleteAsync(id);

            return NoContent();
        }
    }
}
