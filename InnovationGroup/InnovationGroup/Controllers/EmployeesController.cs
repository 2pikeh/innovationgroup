﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using InnovationGroup.Data;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;
using InnovationGroup.Filters;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace InnovationGroup.Controllers
{
    /// <summary>
    /// Exposes endpoints for manipulating an Employee resource
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IEmployeesRepository _repository;
        private readonly IResourceUriService _resourceUriService;
        private readonly IPagedDatasetService _pagingService;

        /// <summary>
        /// Default injection constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="repository">Exposes CRUD operations for manipulating the employee resource</param>
        /// <param name="resourceUriService">Generates URIs for the supported operation on a resource</param>
        /// <param name="pagingService">
        ///     Generates a paging filter that's applied to database
        ///     query before resource collection is returned.
        /// </param>
        public EmployeesController(IMapper mapper,
            IEmployeesRepository repository, 
            IResourceUriService resourceUriService,
            IPagedDatasetService pagingService)
        {
            _mapper = mapper;
            _repository = repository;
            _resourceUriService = resourceUriService;
            _pagingService = pagingService;
        }

        /// <summary>
        /// Gets the employee resource collection based on the
        /// <paramref name="queryParameters"/> provided.
        ///
        /// If none is provided, the default is used. 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Employees
        ///     GET api/Employees?pageNumber=1&pageSize=3
        ///     GET api/Employees?propertiesToInclude=id,name,jobTitle
        ///     GET api/Employees/1?propertiesToInclude=id,name,jobTitle
        ///     GET api/Employees?pageNumber=1&pageSize=3&propertiesToInclude=title,firstName,lastName
        /// </remarks>
        /// <param name="queryParameters">
        ///     Query parameters that can be used in shaping the
        ///     resource's response data. Parameters include pageSize,
        ///     pageNumber and resource fields to include.
        /// </param>
        /// <param name="mediaType">
        ///     the 'ACCEPT' media type - allows an employee's HATEOAS media type for be requested
        /// </param>
        /// <returns></returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">Returned when the input parameters are invalid</response>
        [ServiceFilter(typeof(EmployeesValidationFilter))]
        [HttpGet(Name = StringConstants.GetAllEmployees)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAllEmployees(
            [FromQuery] ResourceQueryParameters queryParameters,
            [FromHeader(Name = "Accept")] string mediaType)
        {
            PagedDataset<Employee> pagedEmployees = _repository.GetAll(queryParameters);

            if (pagedEmployees.TotalCount == 0)
                return NotFound();

            PagingMetadataWithNavigationUris pagingMetadata =
                _pagingService.GetPagingMetadata(pagedEmployees, queryParameters, StringConstants.GetAllEmployees);

            IEnumerable<ExpandoObject> filteredEmployees =
                _mapper.Map<IEnumerable<EmployeesDto>>(pagedEmployees)
                    .ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude);

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                Response?.Headers?.Add("X-Pagination", JsonConvert.SerializeObject(pagingMetadata));

                return Ok(new CollectionResource(
                    filteredEmployees
                        .Select(c => c as IDictionary<string, object>)));
            }

            Response?.Headers?.Add("X-Pagination",
                JsonConvert.SerializeObject(new PagingMetadata(pagingMetadata)));

            IEnumerable<UriDto> collectionUris = _resourceUriService.CreateResourceUri(queryParameters, pagedEmployees.HasNextPage,
                pagedEmployees.HasPreviousPage, StringConstants.GetCustomerById);

            var employeesWithUri = filteredEmployees.Select(e =>
            {
                var employee = e as IDictionary<string, object>;
                var employeeId = (int)employee[nameof(EmployeesDto.Id)];

                IEnumerable<RouteUriMetadata> supportedRoutes =
                    _resourceUriService.GenerateEmployeeRoutes(employeeId, queryParameters);

                IEnumerable<UriDto> itemLinks =
                    _resourceUriService.CreateResourceUri(employeeId.ToString(), supportedRoutes);

                employee.Add(nameof(CollectionResourceWithUri.Uris), itemLinks);

                return employee;
            });

            return Ok(new CollectionResourceWithUri(employeesWithUri, collectionUris));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="queryParameters">
        ///     Parameters that allow paging configuration to be set and the properties that
        ///     should be returned in the response
        /// </param>
        /// <param name="mediaType">Adds support for custom hateoas media type</param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/Employees/11?pageNumber=1&pageSize=3
        ///     GET api/Employees/11?pageNumber=1&pageSize=3&propertiesToInclude=title,firstName,lastName
        ///
        /// </remarks>
        [HttpGet("{id}", Name = StringConstants.GetEmployeeById)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetEmployeeById(int id, 
            [FromQuery]ResourceQueryParameters queryParameters, 
            [FromHeader(Name = "Accept")] string mediaType)
        {
            Employee employee = await _repository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            var mappedEmployee = _mapper.Map<EmployeesDto>(employee);
            var filteredEmployee =
                mappedEmployee.ApplyPropertiesToIncludeTransformation(queryParameters.PropertiesToInclude) as
                    IDictionary<string, object>;

            if (!string.Equals(mediaType, StringConstants.CustomMediaFormatter))
            {
                return Ok(filteredEmployee);
            }

            IEnumerable<RouteUriMetadata> supportedRoutes =
                _resourceUriService.GenerateEmployeeRoutes(mappedEmployee.Id, queryParameters);

            IEnumerable<UriDto> supportedRoutesAsUris =
                _resourceUriService.CreateResourceUri(mappedEmployee.Id.ToString(), supportedRoutes);

            filteredEmployee.Add(nameof(CollectionResourceWithUri.Uris), supportedRoutesAsUris);

            return Ok(filteredEmployee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <remarks>    
        /// POST /api/Employees
        /// {
        ///   "title": "Dr",
        ///    "firstName": "No",
        ///    "lastName": "One",
        ///    "birthDate": "",
        ///    "hireDate": "",
        ///    "phone": 123456
        /// }
        /// 
        /// </remarks>
        [HttpPost(Name = StringConstants.AddNewEmployee)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        public async Task<IActionResult> AddNewEmployee([FromBody] EmployeesCreateDto request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            var mappedToRepoType = _mapper.Map<Employee>(request);
            await _repository.CreateAsync(mappedToRepoType);
            var mappedToDtoType = _mapper.Map<EmployeesDto>(mappedToRepoType);

            return CreatedAtRoute(nameof(GetEmployeeById), 
                new { id = mappedToRepoType.EmployeeId }, mappedToDtoType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifiedEmployee"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        ///
        /// PUT /api/Employees/5
        /// {
        ///   "title": "Dr",
        ///    "firstName": "No",
        ///    "lastName": "One",
        ///    "birthDate": "",
        ///    "hireDate": "",
        ///    "phone": 123456
        /// }
        /// 
        /// </remarks>
        [HttpPut("{id}", Name = StringConstants.ExecuteDeepUpdateOnEmployee)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecuteDeepUpdate(int id, [FromBody] EmployeesUpdateDto modifiedEmployee)
        {
            if (modifiedEmployee == null)
            {
                return BadRequest();
            }

            if (!await _repository.EmployeeExistsAsync(id))
            {
                return NotFound();
            }

            Employee employeeFromRepo = await _repository.GetByIdAsync(id);
            _mapper.Map(modifiedEmployee, employeeFromRepo);
            await _repository.Update(employeeFromRepo);

            return NoContent();
        }

        /// <summary>
        /// Executes a partial update of the employee resource
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patchDocument"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        ///
        /// PATCH api/Employees/11
        /// [
        ///     {
        ///         "value": "That",
        ///         "path": "/firstName",
        ///         "op": "replace",
        ///         "from": "string"
        ///     },
        ///     {
        ///         "value": "Person",
        ///         "path": "/lastName",
        ///         "op": "replace",
        ///         "from": "string"
        ///     }
        /// ]
        /// </remarks>
        [HttpPatch("{id}", Name = StringConstants.ExecutePartialUpdateOnEmployee)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ExecutePartialUpdate(int id,
            [FromBody] JsonPatchDocument<EmployeesUpdateDto> patchDocument)
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            if (!await _repository.EmployeeExistsAsync(id))
            {
                return NotFound();
            }

            var employeeFromRepo = await _repository.GetByIdAsync(id);
            var employeeToPatch = _mapper.Map<EmployeesUpdateDto>(employeeFromRepo);
            patchDocument.ApplyTo(employeeToPatch);

            _mapper.Map(employeeToPatch, employeeFromRepo);
            await _repository.Update(employeeFromRepo);

            return NoContent();
        }

        /// <summary>
        /// Removes an employee resource
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request ->
        /// 
        ///     DELETE: api/Employee/5
        ///
        /// </remarks>
        [HttpDelete("{id}", Name = StringConstants.DeleteEmployee)]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            if (!await _repository.EmployeeExistsAsync(id))
            {
                return NotFound();
            }

            await _repository.DeleteAsync(id);

            return NoContent();
        }
    }
}
