﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using FluentValidation.AspNetCore;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace InnovationGroup
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Register database provider with the IoC container
            services.AddDbContext<InnovationGroupContext>(dbContextOptions =>
            {
                dbContextOptions.UseSqlServer(Configuration.GetConnectionString("innovationgroup"),
                    cfg => cfg.MigrationsAssembly("InnovationGroup"));
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "InnovationGroup API", Version = "v1" });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            StartupHelper.RegisterRepositoryTypes(services);
            StartupHelper.RegisterHelperServices(services); 
            //services.AddHttpCachingMvc();

            services
                .AddMvc(config =>
                {
                    config.ReturnHttpNotAcceptable = true;
                    config.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());

                    var jsonInputFormatter = config.InputFormatters.OfType<JsonInputFormatter>().FirstOrDefault();
                    jsonInputFormatter?.SupportedMediaTypes.Add(
                        Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("text/plain"));

                    var jsonOutputFormatter = config.OutputFormatters.OfType<JsonOutputFormatter>().FirstOrDefault();
                    if (jsonOutputFormatter != null && 
                        !jsonOutputFormatter.SupportedMediaTypes.Contains(StringConstants.CustomMediaFormatter))
                    {
                        jsonOutputFormatter.SupportedMediaTypes.Add(StringConstants.CustomMediaFormatter);
                    }
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })

                //Add support for entity validation
                .AddFluentValidation(fv => {
                    fv.ImplicitlyValidateChildProperties = true;
                    fv.RunDefaultMvcValidationAfterFluentValidationExecutes = true;
                });

            StartupHelper.RegisterAutoMapper(services);
            StartupHelper.RegisterValidationServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IDatabaseSeedDataService dbSeedService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                var logger = loggerFactory.CreateLogger("Global Error Logger");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();

                //Set up global error handler (when ASPNETCORE environment variable is set to "PRODUCTION")
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            logger.LogError(500, exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("Server Error. Try again later.");
                    });
                });
            }

            //AutoMapperConfig.Initialise();
            dbSeedService.Seed();

            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InnovationGroup API");
                
            });

            app.UseMvc();
            //app.UseWelcomePage();
        }
    }
}
