﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Orders;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace InnovationGroup.Filters
{
    /// <inheritdoc />
    /// <summary>
    /// Action filter for validating the query parameters when retrieving Order resource(s)
    /// from the repository.
    /// </summary>
    public class OrdersCollectionValidationFilter : IAsyncActionFilter
    {
        private readonly ITypeHelperService _typeHelperService;
        private readonly IPropertyMappingService _propertyMappingService;

        /// <summary>
        /// Default injection constructor
        /// </summary>
        /// <param name="typeHelperService">
        /// Service that ensures that the properties specified in 'PropertiesToInclude'
        /// are valid properties of the CategoriesDto.
        /// </param>
        /// <param name="propertyMappingService">
        /// Service for validating the existence of a valid mapping between
        /// the DTO and the repository type.
        /// </param>
        public OrdersCollectionValidationFilter(ITypeHelperService typeHelperService, IPropertyMappingService propertyMappingService)
        {
            _typeHelperService = typeHelperService;
            _propertyMappingService = propertyMappingService;
        }

        /// <inheritdoc />
        /// <summary>
        /// Intercept incoming request and ensure that action arguments are valid, otherwise
        /// short-circuit the request pipeline.
        /// </summary>
        /// <param name="context">Request context</param>
        /// <param name="next">continuation action - the target endpoint action.</param>
        /// <returns></returns>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var queryParameters = context.ActionArguments["queryParameters"] as OrderQueryParameters;
            if (queryParameters != null)
            {
                if (!string.IsNullOrWhiteSpace(queryParameters.PropertiesToInclude) & queryParameters.LoadOrderDetails)
                {
                    queryParameters.PropertiesToInclude =
                        $"{queryParameters.PropertiesToInclude},{nameof(OrdersDto.OrderDetails)}";
                }
            }

            if (!_propertyMappingService.ValidMappingExistsFor<OrdersDto, Order>
                (queryParameters?.PropertiesToInclude))
            {
                context.HttpContext.Response.StatusCode = 400;
                context.Result = new BadRequestResult();

                return;
            }

            if (!_typeHelperService.TypeHasProperties<OrdersDto>
                (queryParameters?.PropertiesToInclude))
            {
                context.HttpContext.Response.StatusCode = 400;
                context.Result = new BadRequestResult();

                return;
            }

            await next();
        }
    }
}
