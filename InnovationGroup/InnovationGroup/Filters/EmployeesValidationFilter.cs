﻿using System.Threading.Tasks;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace InnovationGroup.Filters
{
    /// <inheritdoc />
    /// <summary>
    /// Action filter for validating the query parameters when retrieving Employee resource(s)
    /// from the repository.
    /// </summary>
    public class EmployeesValidationFilter : IAsyncActionFilter
    {
        private readonly ITypeHelperService _typeHelperService;
        private readonly IPropertyMappingService _propertyMappingService;

        /// <summary>
        /// Default injection constructor
        /// </summary>
        /// <param name="typeHelperService">
        /// Service that ensures that the properties specified in 'PropertiesToInclude'
        /// are valid properties of the CategoriesDto.
        /// </param>
        /// <param name="propertyMappingService">
        /// Service for validating the existence of a valid mapping between
        /// the DTO and the repository type.
        /// </param>
        public EmployeesValidationFilter(ITypeHelperService typeHelperService, IPropertyMappingService propertyMappingService)
        {
            _typeHelperService = typeHelperService;
            _propertyMappingService = propertyMappingService;
        }

        /// <summary>
        /// Intercept incoming request and ensure that action arguments are valid, otherwise
        /// short-circuit the request pipeline.
        /// </summary>
        /// <param name="context">Request context</param>
        /// <param name="next">continuation action - the target endpoint action.</param>
        /// <returns></returns>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var queryParameters = context.ActionArguments["queryParameters"] as ResourceQueryParameters;

            if (!_propertyMappingService.ValidMappingExistsFor<EmployeesDto, Employee>
                (queryParameters?.PropertiesToInclude))
            {
                context.HttpContext.Response.StatusCode = 400;
                context.Result = new BadRequestResult();

                return;
            }

            if (!_typeHelperService.TypeHasProperties<EmployeesDto>
                (queryParameters?.PropertiesToInclude))
            {
                context.HttpContext.Response.StatusCode = 400;
                context.Result = new BadRequestResult();

                return;
            }

            await next();
        }
    }
}
