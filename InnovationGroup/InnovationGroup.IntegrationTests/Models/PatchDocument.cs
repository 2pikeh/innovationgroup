﻿using Newtonsoft.Json;

namespace InnovationGroup.IntegrationTests.Models
{
    public class PatchDocument
    {
        public PatchDocument(string op, string path, string value)
        {
            Op = op;
            Path = path;
            Value = value;
        }

        [JsonProperty("op")]
        public string Op { get; }

        [JsonProperty("path")]
        public string Path { get; }

        [JsonProperty("value")]
        public string Value { get; }
    }
}
