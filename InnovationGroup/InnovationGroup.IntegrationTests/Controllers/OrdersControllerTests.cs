﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CaseExtensions;
using FluentAssertions;
using InnovationGroup.Domain.Orders;
using InnovationGroup.Domain.Uri;
using Xunit;

namespace InnovationGroup.IntegrationTests.Controllers
{
    public class OrdersControllerTests : IClassFixture<InnovationGroupWebAppFactory>
    {
        private readonly HttpClient _httpClient;

        public OrdersControllerTests(InnovationGroupWebAppFactory factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Theory]
        [InlineData("api/Customer/VINET/Order?PropertiesToInclude=")]
        public async Task When_Given_Properties_To_Include_No_Other_Properties_Should_Be_In_Response(
            string endpoint)
        {
            //Act
            string[] propertiesToInclude = { "id", "orderDate", "shippedDate" };
            endpoint = $"{endpoint}{string.Join(",", propertiesToInclude)}";
            var httpResponse = await _httpClient.GetAsync(endpoint);

            httpResponse.EnsureSuccessStatusCode();
            var orders = (await httpResponse
                .Content.ReadAsAsync<CollectionResource>()).Value;

            //Assert
            var id = propertiesToInclude[0];
            var orderDate = propertiesToInclude[1];
            var shippedDate = propertiesToInclude[2];
            var enumerable = orders as IDictionary<string, object>[] ?? orders.ToArray();

            foreach (var row in enumerable)
            {
                row.Keys.Should().Contain(id);
                row.Keys.Should().Contain(orderDate);
                row.Keys.Should().Contain(shippedDate);
            }
        }

        [Theory]
        [InlineData("api/Customer/VINET/Order?loadOrderDetails=true")]
        public async Task When_Load_Order_Details_Is_True_Should_Include_It_In_Response(
            string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(endpoint);

            httpResponse.EnsureSuccessStatusCode();
            var orders = (await httpResponse
                .Content.ReadAsAsync<CollectionResource>()).Value;

            //Assert
            var enumerable = orders as IDictionary<string, object>[] ?? orders.ToArray();

            foreach (var row in enumerable)
            {
                row.Keys.Should().Contain(nameof(OrdersDto.OrderDetails).ToCamelCase());
            }
        }

        [Theory]
        [InlineData("api/Customer/VINET/Order?PropertiesToInclude={0}&loadOrderDetails=true")]
        public async Task Should_Load_All_PropertiesToInclude_And_OrderDetails_In_Response(
            string endpoint)
        {
            //Act
            string[] propertiesToInclude = {"id", "orderDate", "shippedDate"};
            endpoint = string.Format(endpoint, string.Join(",", propertiesToInclude));

            var httpResponse = await _httpClient.GetAsync(endpoint);

            httpResponse.EnsureSuccessStatusCode();
            var orders = (await httpResponse
                .Content.ReadAsAsync<CollectionResource>()).Value;

            //Assert
            var enumerable = orders as IDictionary<string, object>[] ?? orders.ToArray();

            foreach (var row in enumerable)
            {
                row.Keys.Should().Contain(propertiesToInclude[0]);
                row.Keys.Should().Contain(propertiesToInclude[1]);
                row.Keys.Should().Contain(propertiesToInclude[2]);
                row.Keys.Should().Contain(nameof(OrdersDto.OrderDetails).ToCamelCase());
            }
        }

        [Theory]
        [InlineData("api/Customer/ABCDE/Order")]
        public async Task When_Given_Invalid_CustomerId_Should_Return_NotFound(
            string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(endpoint);

            //Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Theory]
        [InlineData("api/Customer/ABCDE/Order?PropertiesToInclude=InvalidProperty")]
        public async Task When_Given_Invalid_PropertyToInclude_Should_Return_BadRequest(
            string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(endpoint);

            //Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData("api/Customer/VINET/Order")]
        public async Task Order_Resource_Should_Be_Removed(string endpoint)
        {
            //Arrange
            var payload = new OrdersCreateDto
            {
                Id = 99999,
                CustomerId = "VINET"
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var newlyCreated = await httpResponse
                .Content.ReadAsAsync<OrdersDto>();

            httpResponse.EnsureSuccessStatusCode();
            httpResponse = await _httpClient.DeleteAsync($"{endpoint}/{newlyCreated.Id}");

            //Assert
            httpResponse.EnsureSuccessStatusCode();
        }
    }
}
