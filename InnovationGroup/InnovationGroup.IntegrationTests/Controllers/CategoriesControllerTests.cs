﻿using InnovationGroup.Domain.Categories;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using InnovationGroup.Domain.Uri;
using InnovationGroup.IntegrationTests.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace InnovationGroup.IntegrationTests.Controllers
{
    public class CategoriesControllerTests : IClassFixture<InnovationGroupWebAppFactory>
    {
        private readonly HttpClient _httpClient;

        public CategoriesControllerTests(InnovationGroupWebAppFactory factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Theory]
        [InlineData(1, 4, "/api/categories?pageSize={0}&pageNumber={1}")]
        public async Task When_Given_Paging_Settings_In_Request_Should_Override_Server_Settings(int pageNumber,
            int pageSize,
            string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(
                string.Format(endpoint, pageSize, pageNumber));

            httpResponse.EnsureSuccessStatusCode();
            var categories = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            //Assert
            categories.Value.Count().Should().Be(pageSize);
        }

        [Theory]
        [InlineData("api/Categories?PageNumber=1&PageSize=1&PropertiesToInclude=")]
        public async Task When_Given_Properties_To_Include_No_Other_Properties_Should_Be_In_Response(
            string endpoint)
        {
            //Act
            int pageSize = 1;
            string[] propertiesToInclude = {"id", "name"};
            endpoint = $"{endpoint}{string.Join(",", propertiesToInclude)}";
            var httpResponse = await _httpClient.GetAsync(endpoint);

            httpResponse.EnsureSuccessStatusCode();
            var categories = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            //Assert
            var id = propertiesToInclude[0];
            var name = propertiesToInclude[1];

            categories.Value.Count().Should().Be(pageSize);
            categories.Value.First().Keys.Should().Contain(id);
            categories.Value.First().Keys.Should().Contain(name);
        }

        [Theory]
        [InlineData("api/categories")]
        public async Task Add_New_Category_Should_Create_New_Category_Resource(string endpoint)
        {
            //Arrange
            var payload = new CategoriesCreateDto
            {
                Description = "Category created during integration test.",
                Name = "Test Category",
                Picture = new byte[0]
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var newlyCreated = await httpResponse
                .Content.ReadAsAsync<CategoriesDto>();

            //Assert
            httpResponse.EnsureSuccessStatusCode();
            httpResponse.Headers.Location.AbsoluteUri.ToLower().Should()
                .BeEquivalentTo($"http://localhost/{endpoint}/{newlyCreated.Id}");
        }

        [Theory]
        [InlineData("api/categories", "?pageSize={0}&pageNumber={1}")]
        public async Task Update_Operation_Should_Save_All_Properties_of_Category_Resource(
            string baseEndpoint, string pagingConfiguration)
        {
            // Arrange
            var serverResponse = await _httpClient.GetAsync($"{baseEndpoint}{string.Format(pagingConfiguration, 1, 1)}");
            serverResponse.EnsureSuccessStatusCode();

            var category = (await serverResponse.Content.ReadAsAsync<CollectionResource>()).Value.First();
            var categoryId = category[nameof(CategoriesDto.Id).ToLower()];

            var payload = new CategoriesUpdateDto
            {
                Id = int.Parse(categoryId.ToString()),
                Name = "Full Update PUT"
            };

            // Act          
            serverResponse = await _httpClient.PutAsJsonAsync($"{baseEndpoint}/{categoryId}", payload);
            serverResponse.EnsureSuccessStatusCode();
            serverResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NoContent);

            serverResponse = await _httpClient.GetAsync($"{baseEndpoint}/{categoryId}");
            var updatedCategory = await serverResponse.Content.ReadAsAsync<IDictionary<string, object>>();

            //Assert
            serverResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var name = category[nameof(CategoriesDto.Name).ToLower()];
            var updateName = updatedCategory[nameof(CategoriesDto.Name).ToLower()];
            name.Should().NotBe(updateName);

            var desc = category[nameof(CategoriesDto.Description).ToLower()];
            var updatedDesc = updatedCategory[nameof(CategoriesDto.Description).ToLower()];
            desc.Should().NotBe(updatedDesc);
        }

        [Theory]
        [InlineData("api/categories", "?pageSize={0}&pageNumber={1}")]
        public async Task Patch_Operation_Should_Only_Save_Modified_Properties_of_Category_Resource(
            string baseEndpoint,
            string pagingConfiguration)
        {
            // Arrange
            var serverResponse =
                await _httpClient.GetAsync(
                    $"{baseEndpoint}{string.Format(pagingConfiguration, 1, 1)}");

            serverResponse.EnsureSuccessStatusCode();

            var category = (await serverResponse.Content.ReadAsAsync<CollectionResource>())
                .Value
                .First();

            var payload = new List<PatchDocument>
            {
                new PatchDocument("replace", "/Name", "Partial Update")
            };

            // Act
            var categoryId = category[nameof(CategoriesDto.Id).ToLower()];
            serverResponse = await _httpClient.PatchAsync($"{baseEndpoint}/{categoryId}",
                new StringContent(JsonConvert.SerializeObject(payload)));

            serverResponse.EnsureSuccessStatusCode();
            serverResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NoContent);

            serverResponse = await _httpClient.GetAsync($"{baseEndpoint}/{categoryId}");
            var patchedCategory = await serverResponse.Content.ReadAsAsync<IDictionary<string, object>>();

            // Assert
            serverResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var name = category[nameof(CategoriesDto.Name).ToLower()];
            var nameInPatched = patchedCategory[nameof(CategoriesDto.Name).ToLower()];
            name.Should().NotBe(nameInPatched);

            var desc = category[nameof(CategoriesDto.Description).ToLower()];
            var descInPatched = patchedCategory[nameof(CategoriesDto.Description).ToLower()];
            desc.Should().Be(descInPatched);
        }

        [Theory]
        [InlineData("api/categories")]
        public async Task Adding_New_Category_Fails_Validation_When_Name_Exceeds_Maximum_Length(string endpoint)
        {
            //Arrange
            const string expectedError = "'Name' must be between 5 and 15 characters. You entered 40 characters.";
            var payload = new CategoriesCreateDto
            {
                Description = "Category created during integration test.",
                Name = "An Unbelievably Long Name For a Category",
                Picture = new byte[0]
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var errorResponse =
                JObject.FromObject(JsonConvert.DeserializeObject(await httpResponse.Content.ReadAsStringAsync()));

            //Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var serverError = errorResponse["errors"]["name"][0].ToString();
            expectedError.Should().Be(serverError);
        }

        [Theory]
        [InlineData("api/categories")]
        public async Task Category_Resource_Should_Be_Removed(string endpoint)
        {
            //Arrange
            var payload = new CategoriesCreateDto
            {
                Description = "Category created for the sake of deleting.",
                Name = "DeleteTest",
                Picture = new byte[0]
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var newlyCreated = await httpResponse
                .Content.ReadAsAsync<CategoriesDto>();

            httpResponse.EnsureSuccessStatusCode();
            httpResponse = await _httpClient.DeleteAsync($"{endpoint}/{newlyCreated.Id}");

            //Assert
            httpResponse.EnsureSuccessStatusCode();
        }


    }
}
