﻿using InnovationGroup.Domain.Customers;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CaseExtensions;
using FluentAssertions;
using InnovationGroup.Domain.Uri;
using InnovationGroup.IntegrationTests.Models;
using Newtonsoft.Json;
using Xunit;

namespace InnovationGroup.IntegrationTests.Controllers
{
    public class CustomerControllerTests : IClassFixture<InnovationGroupWebAppFactory>
    {
        private readonly HttpClient _httpClient;

        public CustomerControllerTests(InnovationGroupWebAppFactory factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Theory]
        [InlineData(1, 18, "/api/customers?pageSize={0}&pageNumber={1}")]
        public async Task When_Given_Paging_Settings_In_Request_Should_Override_Server_Settings(
            int pageNumber, int pageSize, string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(
                string.Format(endpoint, pageSize, pageNumber));

            httpResponse.EnsureSuccessStatusCode();
            var customers = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            //Assert
            customers.Value.Count().Should().Be(pageSize);
        }


        [Theory]
        [InlineData(1, 2, "/api/customers?pageSize={0}&pageNumber={1}")]
        public async Task Customers_GetCustomers_Returns_Json_Response_For_Json_MediaType(int pageNumber, int pageSize, string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(
                string.Format(endpoint, pageSize, pageNumber));

            httpResponse.EnsureSuccessStatusCode();

            var customers = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            //Assert
            customers.Value.Count().Should().Be(pageSize);
        }

        [Theory]
        [InlineData(10, "/api/customers")]
        public async Task When_No_Paging_Setting_Provided_Server_Should_Apply_Default_Paging_Settings(
            int defaultPageSize, string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(endpoint);

            //Assert
            httpResponse.EnsureSuccessStatusCode();
            var customers = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            customers.Value.Count().Should().Be(defaultPageSize);
        }

        [Theory]
        [InlineData(20, 30, 1, "/api/customers?pageSize={0}&pageNumber={1}")]
        public async Task PerRequest_The_Maximum_Rows_In_CustomersCollection_Cannot_Exceed_ServerMaximum(
            int maximumPageSize, int pageSize, int pageNumber, string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(string.Format(endpoint, pageSize, pageNumber));

            //Assert
            httpResponse.EnsureSuccessStatusCode();
            var customers = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            pageSize.Should().BeGreaterThan(maximumPageSize);
            customers.Value.Count().Should().Be(maximumPageSize);
        }

        [Theory]
        [InlineData("api/customers")]
        public async Task Adding_New_Should_Create_New_Customer_Resource(string endpoint)
        {
            //Arrange
            var payload = new CustomersCreateDto
            {
                Id = "EKFP",
                Company = "Integration Test",
                ContactName = "Test Contact",
                ContactTitle = "Dr."
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var newlyCreated = await httpResponse
                .Content.ReadAsAsync<CustomersDto>();

            //Assert
            httpResponse.EnsureSuccessStatusCode();
            httpResponse.Headers.Location.AbsoluteUri.Should()
                .BeEquivalentTo($"http://localhost/{endpoint}/{newlyCreated.Id}");
        }

        [Theory]
        [InlineData("api/customers")]
        public async Task Adding_New_Customer_Fails_Validation_When_Missing_CustomerId(string endpoint)
        {
            //Arrange
            var errorMessage = "'Id' must not be empty.";
            var payload = new CustomersCreateDto
            {
                Company = "Integration Test",
                ContactName = "Test Contact",
                ContactTitle = "Dr."
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var errorResponse =
                JObject.FromObject(JsonConvert.DeserializeObject(await httpResponse.Content.ReadAsStringAsync()));

            //Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Should().BeEquivalentTo(errorResponse["errors"]["id"][0].ToString());
        }

        [Theory]
        [InlineData("api/customers", "?pageSize={0}&pageNumber={1}")]
        public async Task Update_Operation_Should_Save_All_Properties_Of_Customer_Resource(
            string baseEndpoint, string pagingConfiguration)
        {
            // Arrange
            var getCustomerResponse = await _httpClient.GetAsync($"{baseEndpoint}{string.Format(pagingConfiguration, 1, 1)}");
            getCustomerResponse.EnsureSuccessStatusCode();

            IDictionary<string, object> customer =
                (await getCustomerResponse.Content.ReadAsAsync<CollectionResource>()).Value.First();

            var customerId = customer[nameof(CustomersDto.Id).ToCamelCase()].ToString();
            var payload = new CustomersUpdateDto
            {
                Id = customerId,
                Company = "Full Update"
            };

            //Act
            var httpResponse = await _httpClient.PutAsJsonAsync($"{baseEndpoint}/{customerId}", payload);
            httpResponse.EnsureSuccessStatusCode();
            getCustomerResponse = await _httpClient.GetAsync($"{baseEndpoint}/{customerId}");
            var updatedCustomer = await getCustomerResponse.Content.ReadAsAsync<IDictionary<string, object>>();

            //Assert
            httpResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NoContent);

            customer[nameof(CustomersDto.Company).ToCamelCase()].Should()
                .NotBe(updatedCustomer[nameof(CustomersDto.Company).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.ContactName).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.ContactName).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();

            updatedCustomer[nameof(CustomersDto.ContactTitle).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.ContactTitle).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();

            updatedCustomer[nameof(CustomersDto.Address).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.Address).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();

            updatedCustomer[nameof(CustomersDto.City).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.City).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();
            updatedCustomer[nameof(CustomersDto.PostCode).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.PostCode).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();

            updatedCustomer[nameof(CustomersDto.Country).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.Country).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();
            updatedCustomer[nameof(CustomersDto.Phone).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();

            customer[nameof(CustomersDto.Phone).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();
            updatedCustomer[nameof(CustomersDto.Fax).ToCamelCase()]?.ToString().Should().BeNullOrWhiteSpace();
            customer[nameof(CustomersDto.Fax).ToCamelCase()]?.ToString().Should().NotBeNullOrWhiteSpace();
        }

        [Theory]
        [InlineData("api/customers", "?pageSize={0}&pageNumber={1}")]
        public async Task Update_Operation_Should_Only_Save_Modified_Properties_Of_Customer_Resource(
            string baseEndpoint, string pagingConfiguration)
        {
            // Arrange
            var getCustomerResponse = await _httpClient.GetAsync($"{baseEndpoint}{string.Format(pagingConfiguration, 1, 1)}");
            getCustomerResponse.EnsureSuccessStatusCode();

            var customer = (await getCustomerResponse.Content.ReadAsAsync<CollectionResource>()).Value.First();
            var payload = new List<PatchDocument>
            {
                new PatchDocument("replace", "/Company", "Partial Update")
            };

            // Act
            var customerId = customer[nameof(CustomersDto.Id).ToCamelCase()];
            var httpResponse = await _httpClient.PatchAsync($"{baseEndpoint}/{customerId}",
                new StringContent(JsonConvert.SerializeObject(payload)));

            httpResponse.EnsureSuccessStatusCode();
            getCustomerResponse = await _httpClient.GetAsync($"{baseEndpoint}/{customerId}");
            var updatedCustomer = await getCustomerResponse.Content.ReadAsAsync<IDictionary<string, object>>();

            // Assert
            httpResponse.StatusCode.Should().BeEquivalentTo( HttpStatusCode.NoContent);

            customer[nameof(CustomersDto.Company).ToCamelCase()].Should()
                .NotBe(updatedCustomer[nameof(CustomersDto.Company).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.ContactName).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.ContactName).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.ContactTitle).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.ContactTitle).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.Address).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.Address).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.City).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.City).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.PostCode).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.PostCode).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.Country).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.Country).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.Phone).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.Phone).ToCamelCase()]);

            updatedCustomer[nameof(CustomersDto.Fax).ToCamelCase()].Should()
                .BeEquivalentTo(customer[nameof(CustomersDto.Fax).ToCamelCase()]);
        }

        [Theory]
        [InlineData("api/customers")]
        public async Task Adding_New_Customer_Fails_Validation_When_Missing_Company(string endpoint)
        {
            //Arrange
            const string expectedErrorMessage = "The Company field is required."; //"'Company' must not be empty.";
            var payload = new CustomersCreateDto
            {
                Id = "CXRE",
                Company = ""
            };

            //Act
            var httpResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var errorResponse =
                JObject.FromObject(JsonConvert.DeserializeObject(await httpResponse.Content.ReadAsStringAsync()));

            //Assert
            httpResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            expectedErrorMessage.Should().BeEquivalentTo((errorResponse["errors"]["company"][0]).ToString());
        }

        [Theory]
        [InlineData(1, 1, "api/customers")]
        public async Task Customer_Resource_Should_Be_Deleted(int pageSize, int pageNumber, string endpoint)
        {
            //Arrange
            var httpResponse = await _httpClient.GetAsync(
                $"{endpoint}?pageSize={pageSize}&pageNumber={pageNumber}");

            var customer = (await httpResponse
                .Content.ReadAsAsync<CollectionResource>())
                .Value
                .First();

            //Act
            var customerId = customer[nameof(CustomersDto.Id).ToCamelCase()];
            httpResponse = await _httpClient.DeleteAsync($"{endpoint}/{customerId}");

            //Assert
            httpResponse.EnsureSuccessStatusCode();
        }
    }
}
