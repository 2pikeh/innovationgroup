﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CaseExtensions;
using FluentAssertions;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Uri;
using InnovationGroup.IntegrationTests.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace InnovationGroup.IntegrationTests.Controllers
{
    public class EmployeesControllerTests : IClassFixture<InnovationGroupWebAppFactory>
    {
        private readonly HttpClient _httpClient;

        public EmployeesControllerTests(InnovationGroupWebAppFactory factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Theory]
        [InlineData(1, 5, "/api/employees?pageSize={0}&pageNumber={1}")]
        public async Task When_Given_Paging_Settings_In_Request_Should_Override_Server_Settings(
            int pageNumber, 
            int pageSize, 
            string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(
                string.Format(endpoint, pageSize, pageNumber));

            httpResponse.EnsureSuccessStatusCode();
            var employees = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            //Assert
            employees.Value.Count().Should().Be(pageSize);
        }


        [Theory]
        [InlineData(1, 2, "/api/employees?pageSize={0}&pageNumber={1}")]
        public async Task Employees_GetEmployees_Returns_Json_Response_For_Json_MediaType(int pageNumber, int pageSize, string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(
                string.Format(endpoint, pageSize, pageNumber));

            httpResponse.EnsureSuccessStatusCode();

            var employees = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            //Assert
            employees.Value.Count().Should().Be(pageSize);
        }

        [Theory]
        [InlineData(9, "/api/employees")]
        public async Task When_No_Paging_Setting_Provided_Server_Should_Apply_Default_Paging_Settings(
            int totalNumberOfEmployees, string endpoint)
        {
            //Act
            var httpResponse = await _httpClient.GetAsync(endpoint);

            //Assert
            httpResponse.EnsureSuccessStatusCode();
            var employees = await httpResponse
                .Content.ReadAsAsync<CollectionResource>();

            employees.Value.Count().Should().Be(totalNumberOfEmployees);
        }

        [Theory]
        [InlineData(9, 30, 1, "/api/employees?pageSize={0}&pageNumber={1}")]
        public async Task PerRequest_The_Maximum_Rows_In_EmployeeCollection_Cannot_Exceed_ServerMaximum(
            int maximumServerPageSize, int pageSize, int pageNumber, string endpoint)
        {
            //Act
            var serverResponse = await _httpClient.GetAsync(string.Format(endpoint, pageSize, pageNumber));
            
            //Assert
            serverResponse.EnsureSuccessStatusCode();
            var employees = await serverResponse
                .Content.ReadAsAsync<CollectionResource>();

            pageSize.Should().BeGreaterThan(maximumServerPageSize);
            employees.Value.Count().Should().Be(maximumServerPageSize);
        }

        [Theory]
        [InlineData("api/employees")]
        public async Task AddNewEmployee_Should_Create_New_Employee_Resource(string endpoint)
        {
            //Arrange
            var payload = new EmployeesCreateDto()
            {
                FirstName = "fname",
                LastName = "lname"
            };

            //Act
            var serverResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);
            var newlyCreated = await serverResponse.Content.ReadAsAsync<EmployeesDto>();

            //Assert
            serverResponse.EnsureSuccessStatusCode();
            serverResponse.Headers.Location.AbsoluteUri.Should()
                .BeEquivalentTo($"http://localhost/{endpoint}/{newlyCreated.Id}");
        }

        [Theory]
        [InlineData("api/employees", "?pageSize={0}&pageNumber={1}")]
        public async Task Update_Operation_Should_Save_All_Properties_Of_Employee_Resource
            (string baseEndpoint, string pagingConfiguration)
        {
            // Arrange
            var serverResponse =
                await _httpClient.GetAsync($"{baseEndpoint}{string.Format(pagingConfiguration, 1, 1)}");

            serverResponse.EnsureSuccessStatusCode();
            var employee = (await serverResponse.Content.ReadAsAsync<CollectionResource>()).Value.First();
            var employeeId = employee[nameof(EmployeesDto.Id).ToCamelCase()];

            var payload = new EmployeesUpdateDto
            {
                EmployeeId = int.Parse(employeeId.ToString()),
                FirstName = "fname",
                LastName = "lname"
            };

            //Act
            serverResponse = await _httpClient.PutAsJsonAsync($"{baseEndpoint}/{employeeId}", payload);
            serverResponse.EnsureSuccessStatusCode();

            serverResponse = await _httpClient.GetAsync($"{baseEndpoint}/{employeeId}");
            var updatedEmployee = await serverResponse.Content.ReadAsAsync<IDictionary<string, object>>();

            //Assert
            var name = employee[nameof(EmployeesDto.Name).ToCamelCase()];
            var updatedName = updatedEmployee[nameof(EmployeesDto.Name).ToCamelCase()];
            name.Should().NotBe(updatedName);

            var address = employee[nameof(EmployeesDto.Address).ToCamelCase()].ToString();
            var updatedAddress = updatedEmployee[nameof(EmployeesDto.Address).ToCamelCase()]?.ToString();
            address.Should().NotBeNullOrWhiteSpace();
            updatedAddress.Should().BeNullOrWhiteSpace();

            var city = employee[nameof(EmployeesDto.City).ToCamelCase()].ToString();
            var updatedCity = updatedEmployee[nameof(EmployeesDto.City).ToCamelCase()]?.ToString();
            city.Should().NotBeNullOrWhiteSpace();
            updatedCity.Should().BeNullOrWhiteSpace();

            var postCode = employee[nameof(EmployeesDto.PostCode).ToCamelCase()]?.ToString();
            var updatedPostCode = updatedEmployee[nameof(EmployeesDto.PostCode).ToCamelCase()]?.ToString();
            postCode.Should().BeNullOrWhiteSpace();
            updatedPostCode.Should().BeNullOrWhiteSpace();

            var country = employee[nameof(EmployeesDto.Country).ToCamelCase()].ToString();
            var updatedCountry = updatedEmployee[nameof(EmployeesDto.Country).ToCamelCase()]?.ToString();
            country.Should().NotBeNullOrWhiteSpace();
            updatedCountry.Should().BeNullOrWhiteSpace();           
        }

        [Theory]
        [InlineData("api/employees", "?pageSize={0}&pageNumber={1}")]
        public async Task Update_Operation_Should_Only_Save_Modified_Properties_Of_Employee_Resource
            (string baseEndpoint, string pagingConfiguration)
        {
            // Arrange
            var serverResponse =
                await _httpClient.GetAsync($"{baseEndpoint}{string.Format(pagingConfiguration, 1, 1)}");

            serverResponse.EnsureSuccessStatusCode();

            var employee = 
                (await serverResponse
                    .Content.ReadAsAsync<CollectionResource>())
                    .Value
                    .First();
            var employeeId = employee[nameof(EmployeesDto.Id).ToCamelCase()];

            var payload = new List<PatchDocument>
            {
                new PatchDocument("replace", "/Country", "Switzerland")
            };
            
            // Act
            serverResponse = await _httpClient.PatchAsync($"{baseEndpoint}/{employeeId}",
                new StringContent(JsonConvert.SerializeObject(payload)));

            serverResponse.EnsureSuccessStatusCode();

            serverResponse = await _httpClient.GetAsync($"{baseEndpoint}/{employeeId}");
            var patched = await serverResponse.Content.ReadAsAsync<IDictionary<string, object>>();

            // Assert
            var country = employee[nameof(EmployeesDto.Country).ToCamelCase()];
            var countryInPatched = patched[nameof(EmployeesDto.Id).ToCamelCase()];
            country.Should().NotBe(countryInPatched);

            var address = employee[nameof(EmployeesDto.Address).ToCamelCase()];
            var addressInPatched = patched[nameof(EmployeesDto.Address).ToCamelCase()];
            address.Should().Be(addressInPatched);

            var city = employee[nameof(EmployeesDto.City).ToCamelCase()];
            var cityInPatched = patched[nameof(EmployeesDto.City).ToCamelCase()];
            city.Should().Be(cityInPatched);
        }

        [Theory]
        [InlineData("api/employees")]
        public async Task AddNewEmployee_Fails_Validation_When_Missing_FirstName(string endpoint)
        {
            //Arrange
            const string expectedErrorResponse = "The FirstName field is required.";
            var payload = new EmployeesCreateDto
            {
                LastName = "lname"
            };

            //Act
            var serverResponse = await _httpClient.PostAsJsonAsync(endpoint, payload);

            var errorResponse = JObject.FromObject(
                JsonConvert.DeserializeObject(
                    await serverResponse.Content.ReadAsStringAsync()));

            var serverErrorResponse = errorResponse["errors"]["firstName"][0].ToString();

            //Assert
            serverResponse.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            expectedErrorResponse.Should().BeEquivalentTo(serverErrorResponse);
        }

        [Theory]
        [InlineData(1, 1, "api/employees")]
        public async Task Employee_Resource_Should_Be_Deleted(int pageSize, int pageNumber, string endpoint)
        {
            //Arrange
            var serverResponse = await _httpClient.GetAsync($"{endpoint}?pageSize={pageSize}&pageNumber={pageNumber}");
            var employee = (await serverResponse.Content.ReadAsAsync<CollectionResource>()).Value.First();
            var employeeId = employee[nameof(EmployeesDto.Id).ToCamelCase()];

            //Act
            serverResponse = await _httpClient.DeleteAsync($"{endpoint}/{employeeId}");

            //Assert
            serverResponse.EnsureSuccessStatusCode();
            serverResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }
    }
}
