﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Linq;
using AutoMapper;
using InnovationGroup.Data.DataProvider;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace InnovationGroup.IntegrationTests
{
    public class InnovationGroupWebAppFactory : WebApplicationFactory<Startup>
    {
        protected override IWebHostBuilder CreateWebHostBuilder() =>
            WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>();

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var serviceProvider =
                    new ServiceCollection()
                        .AddEntityFrameworkInMemoryDatabase()
                        .BuildServiceProvider();

                // Add a database context using an in-memory database for testing.
                services.AddDbContext<InnovationGroupContext>(options =>
                {
                    options.UseInMemoryDatabase("InnovationGroupIntegrationTests");
                    options.UseInternalServiceProvider(serviceProvider);
                });

                services
                    .AddMvc(config =>
                    {
                        config.ReturnHttpNotAcceptable = true;
                        config.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());

                        var jsonInputFormatter = config.InputFormatters.OfType<JsonInputFormatter>().FirstOrDefault();
                        jsonInputFormatter?.SupportedMediaTypes.Add(
                            Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("text/plain"));

                        var jsonOutputFormatter =
                            config.OutputFormatters.OfType<JsonOutputFormatter>().FirstOrDefault();
                        jsonOutputFormatter?.SupportedMediaTypes.Add(StringConstants.CustomMediaFormatter);
                    });

                StartupHelper.RegisterAutoMapper(services);

                services.Replace(
                    new ServiceDescriptor(
                        typeof(IDatabaseSeedDataService),
                            typeof(TestDbSeedDataService), ServiceLifetime.Scoped));

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var logger = scopedServices.GetRequiredService<ILogger<InnovationGroupWebAppFactory>>();

                    try
                    {
                        // Seed the database with some specific test data.
                        scopedServices
                            .GetRequiredService<IDatabaseSeedDataService>()
                            .Seed();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "An error occurred seeding the " + "database with test messages. Error: {ex.Message}");
                    }
                }
            });
        }
    }
}
