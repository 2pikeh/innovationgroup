﻿using System;

namespace InnovationGroup.Domain.Employees
{
    public class EmployeesDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string JobTitle { get; set; }

        public string TitleOfCourtesy { get; set; }

        public DateTime? DoB { get; set; }

        public DateTime? HireDate { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public string HomePhone { get; set; }

        public string OfficeExt { get; set; }

        public byte[] Photo { get; set; }

        public string Notes { get; set; }
    }
}
