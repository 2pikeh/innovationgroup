﻿namespace InnovationGroup.Domain.Services
{
    public class PagingMetadata : PaginationMetadataBase
    {
        public PagingMetadata(int pageSize, int currentPage, int totalPages, int totalCount) 
            : base(pageSize, currentPage, totalPages, totalCount)
        {
        }

        public PagingMetadata(PagingMetadataWithNavigationUris metadata)
            : this(metadata.PageSize, metadata.CurrentPage, metadata.TotalPages, metadata.TotalCount)
        {

        }
    }
}
