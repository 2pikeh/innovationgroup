﻿namespace InnovationGroup.Domain.Services
{
    public class PagingQueryParameters
    {
        private const int MaxPageSize = 20;
        private int _defaultPageSize = 10;

        public int PageNumber { get; set; } = 1;

        public int PageSize
        {
            get => _defaultPageSize;
            set => _defaultPageSize = value > MaxPageSize ? MaxPageSize : value;
        }
    }
}
