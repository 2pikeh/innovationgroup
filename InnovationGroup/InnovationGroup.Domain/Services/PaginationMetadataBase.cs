﻿namespace InnovationGroup.Domain.Services
{
    public abstract class PaginationMetadataBase
    {
        public PaginationMetadataBase(int pageSize, int currentPage, int totalPages, int totalCount)
        {
            PageSize = pageSize;
            CurrentPage = currentPage;
            TotalPages = totalPages;
            TotalCount = totalCount;
        }

        public int PageSize { get; }
        public int CurrentPage { get; }
        public int TotalPages { get; }
        public int TotalCount { get; }
    }
}
