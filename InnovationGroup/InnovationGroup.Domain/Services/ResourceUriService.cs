﻿using System.Collections.Generic;
using System.Linq;
using InnovationGroup.Domain.Uri;
using Microsoft.AspNetCore.Mvc;

namespace InnovationGroup.Domain.Services
{
    public class ResourceUriService : IResourceUriService
    {
        private readonly IUrlHelper _urlHelper;

        public ResourceUriService(IUrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }

        public string CreateResourceUri(string routeName, dynamic routeValues) =>
            _urlHelper.Link(routeName, routeValues);

        public string CreateResourceUri(ResourceQueryParameters queryParameters, ResourceUriType type,
            string routeName)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link(routeName,
                        new
                        {
                            pageNumber = queryParameters.PageNumber - 1,
                            pageSize = queryParameters.PageSize
                        });

                case ResourceUriType.NextPage:
                    return _urlHelper.Link(routeName,
                        new
                        {
                            pageNumber = queryParameters.PageNumber + 1,
                            pageSize = queryParameters.PageSize
                        });

                case ResourceUriType.CurrentPage:
                    return _urlHelper.Link(routeName,
                        new
                        {
                            pageNumber = queryParameters.PageNumber,
                            pageSize = queryParameters.PageSize
                        });

                default:
                    break;
            }

            return string.Empty;
        }

        public IEnumerable<UriDto> CreateResourceUri(ResourceQueryParameters queryParameters, bool hasPreviousPage,
            bool hasNextPage, string routeName)
        {
            var uris = new List<UriDto>
            {
                new UriDto(CreateResourceUri(
                    queryParameters, ResourceUriType.CurrentPage, routeName), "self", "GET")
            };

            if (hasPreviousPage)
            {
                uris.Add(new UriDto(CreateResourceUri(
                    queryParameters, ResourceUriType.PreviousPage, routeName), "previousPage", "GET"));
            }

            if (hasNextPage)
            {
                uris.Add(new UriDto(CreateResourceUri(
                    queryParameters, ResourceUriType.NextPage, routeName), "nextPage", "GET"));
            }

            return uris;
        }

        public IEnumerable<UriDto> CreateResourceUri(string id, IEnumerable<RouteUriMetadata> routeConfiguration)
        {
            return (from RouteUriMetadata config in routeConfiguration
                    select new UriDto(CreateResourceUri(config.RouteName, config.RouteValues), config.RouteReference,
                        config.RouteMethod))
                .ToList();
        }

        public IEnumerable<RouteUriMetadata> GenerateCustomerRoutes(string customerId, ResourceQueryParameters queryParameters)
        {
            return new List<RouteUriMetadata>
            {
                new RouteUriMetadata(StringConstants.GetAllCustomers, new {queryParameters.PropertiesToInclude},
                    "self", "GET"),

                new RouteUriMetadata(StringConstants.GetAllCustomers,
                    new
                    {
                        queryParameters.PropertiesToInclude,
                        pageNumber = queryParameters.PageNumber,
                        pageSize = queryParameters.PageSize
                    }, "get_all_customers_based_on_query_parameters", "GET"),

                new RouteUriMetadata(StringConstants.GetCustomerById,
                    new {id = customerId}, "get_customer_by_id", "GET"),

                new RouteUriMetadata(StringConstants.GetCustomerById,
                    new {id = customerId, queryParameters.PropertiesToInclude},
                    "get_filtered_customer_by_properties_to_display", "GET"),

                new RouteUriMetadata(StringConstants.ExecutePartialUpdateOnCustomer, new {id = customerId},
                    "update_modified_properties_only", "PATCH"),

                new RouteUriMetadata(StringConstants.ExecuteDeepUpdateOnCustomer, new {id = customerId},
                    "update_all_properties", "PUT"),

                new RouteUriMetadata(StringConstants.AddNewCustomer, new {},
                    "add_new_customer", "POST"),

                new RouteUriMetadata(StringConstants.DeleteCustomer, new {id = customerId},
                    "delete_customer", "DELETE")
            };
        }

        public IEnumerable<RouteUriMetadata> GenerateEmployeeRoutes(int employeeId, ResourceQueryParameters queryParameters)
        {
            return new List<RouteUriMetadata>
            {
                new RouteUriMetadata(StringConstants.GetAllEmployees, new {queryParameters.PropertiesToInclude},
                    "self", "GET"),

                new RouteUriMetadata(StringConstants.GetAllEmployees,
                    new
                    {
                        queryParameters.PropertiesToInclude,
                        pageNumber = queryParameters.PageNumber,
                        pageSize = queryParameters.PageSize
                    }, "get_all_employees_based_on_query_parameters", "GET"),

                new RouteUriMetadata(StringConstants.GetEmployeeById,
                    new {id = employeeId}, "get_employee_by_id", "GET"),

                new RouteUriMetadata(StringConstants.GetEmployeeById,
                    new {id = employeeId, queryParameters.PropertiesToInclude},
                    "get_filtered_employee_by_properties_to_display", "GET"),

                new RouteUriMetadata(StringConstants.ExecutePartialUpdateOnEmployee, new {id = employeeId},
                    "update_modified_properties_only", "PATCH"),

                new RouteUriMetadata(StringConstants.ExecuteDeepUpdateOnEmployee, new {id = employeeId},
                    "update_all_properties", "PUT"),

                new RouteUriMetadata(StringConstants.DeleteEmployee, new {id = employeeId},
                    "delete_employee", "DELETE")
            };
        }

        public IEnumerable<RouteUriMetadata> GenerateOrderRoutes(int orderId, string customerId, ResourceQueryParameters queryParameters)
        {
            return new List<RouteUriMetadata>
            {
                new RouteUriMetadata(StringConstants.GetAllOrders, new {queryParameters.PropertiesToInclude},
                    "self", "GET"),

                new RouteUriMetadata(StringConstants.GetAllOrders,
                    new
                    {
                        queryParameters.PropertiesToInclude,
                        pageNumber = queryParameters.PageNumber,
                        pageSize = queryParameters.PageSize
                    }, "get_all_orders_based_on_query_parameters", "GET"),

                new RouteUriMetadata(StringConstants.GetAllOrdersForCustomer,
                    new
                    {
                        customerId,
                        queryParameters.PropertiesToInclude,
                        pageNumber = queryParameters.PageNumber,
                        pageSize = queryParameters.PageSize
                    }, "get_all_orders_for_customer", "GET"),

                new RouteUriMetadata(StringConstants.GetOrderForCustomer,
                    new {customerId, orderId, queryParameters.PropertiesToInclude},
                    "get_order_for_customer", "GET"),

                new RouteUriMetadata(StringConstants.GetOrderById,
                    new {id = orderId}, "get_order_by_id", "GET"),

                new RouteUriMetadata(StringConstants.GetOrderById,
                    new {id = orderId, queryParameters.PropertiesToInclude},
                    "get_filtered_orderId_by_properties_to_display", "GET"),

                new RouteUriMetadata(StringConstants.GetCustomerOrder,
                    new {id = orderId, queryParameters.PropertiesToInclude},
                    "get_filtered_orderId_by_properties_to_display", "GET"),

                new RouteUriMetadata(StringConstants.ExecutePartialUpdateOnOrder, new {id = orderId},
                    "update_modified_properties_only", "PATCH"),

                new RouteUriMetadata(StringConstants.DeleteOrder, new {id = orderId},
                    "delete_order", "DELETE")
            };
        }

        public IEnumerable<RouteUriMetadata> GenerateCategoriesRoutes(int categoryId, ResourceQueryParameters queryParameters)
        {
            return new List<RouteUriMetadata>
            {
                new RouteUriMetadata(StringConstants.GetAllCategories, new {queryParameters.PropertiesToInclude},
                    "self", "GET"),

                new RouteUriMetadata(StringConstants.GetAllCategories,
                    new
                    {
                        queryParameters.PropertiesToInclude,
                        pageNumber = queryParameters.PageNumber,
                        pageSize = queryParameters.PageSize
                    }, "get_all_categories_based_on_query_parameters", "GET"),

                new RouteUriMetadata(StringConstants.GetCategoryById,
                    new {id = categoryId}, "get_category_by_id", "GET"),

                new RouteUriMetadata(StringConstants.GetCategoryById,
                    new {id = categoryId, queryParameters.PropertiesToInclude}, 
                        "get_filtered_category_by_properties_to_display", "GET"),

                new RouteUriMetadata(StringConstants.ExecutePartialUpdateOnCategory, new {id = categoryId},
                    "update_modified_properties_only", "PATCH"),

                new RouteUriMetadata(StringConstants.ExecuteDeepUpdateOnCategory, new {id = categoryId},
                    "update_all_properties", "PUT"),

                new RouteUriMetadata(StringConstants.AddNewCategory, new {},
                    "add_new_category", "POST"),

                new RouteUriMetadata(StringConstants.DeleteCategory, new {id = categoryId},
                    "delete_category", "DELETE")
            };
        }
    }
}
