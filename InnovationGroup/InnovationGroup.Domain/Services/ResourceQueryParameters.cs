﻿namespace InnovationGroup.Domain.Services
{
    public class ResourceQueryParameters : PagingQueryParameters
    {
        public string PropertiesToInclude { get; set; }
    }
}
