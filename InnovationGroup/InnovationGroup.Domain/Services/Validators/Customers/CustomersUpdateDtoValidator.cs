﻿using FluentValidation;
using InnovationGroup.Domain.Customers;

namespace InnovationGroup.Domain.Services.Validators.Customers
{
    public class CustomersUpdateDtoValidator : AbstractValidator<CustomersUpdateDto>
    {
        public CustomersUpdateDtoValidator()
        {
            RuleFor(c => c.Id).NotEmpty().MaximumLength(5);
            RuleFor(c => c.Company).NotEmpty().MaximumLength(30);
            RuleFor(c => c.ContactTitle).MaximumLength(30);
            RuleFor(c => c.ContactName).MaximumLength(30);
            RuleFor(c => c.Address).MaximumLength(60);
            RuleFor(c => c.City).MaximumLength(15);
            RuleFor(c => c.County).MaximumLength(15);
            RuleFor(c => c.PostCode).MaximumLength(10);
            RuleFor(c => c.Country).MaximumLength(15);
            RuleFor(c => c.Phone).MaximumLength(20);
            RuleFor(c => c.Fax).MaximumLength(20);
        }
    }
}
