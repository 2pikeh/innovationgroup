﻿using FluentValidation;
using InnovationGroup.Domain.Employees;

namespace InnovationGroup.Domain.Services.Validators.Employees
{
    public class EmployeeUpdateDtoValidator : AbstractValidator<EmployeesUpdateDto>
    {
        public EmployeeUpdateDtoValidator()
        {
            RuleFor(c => c.LastName).NotEmpty().Length(10, 30);
            RuleFor(c => c.FirstName).NotEmpty().Length(10, 30);
            RuleFor(c => c.Title).MaximumLength(30);
            RuleFor(c => c.Address).MaximumLength(60);
            RuleFor(c => c.City).MaximumLength(15);
            RuleFor(c => c.Region).MaximumLength(15);
            RuleFor(c => c.PostalCode).MaximumLength(10);
            RuleFor(c => c.Country).MaximumLength(15);
            RuleFor(c => c.HomePhone).MaximumLength(20);
            RuleFor(c => c.Extension).MaximumLength(4);
        }
    }
}
