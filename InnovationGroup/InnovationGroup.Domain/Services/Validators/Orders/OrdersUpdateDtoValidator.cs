﻿using FluentValidation;
using InnovationGroup.Domain.Orders;

namespace InnovationGroup.Domain.Services.Validators.Orders
{
    public class OrdersUpdateDtoValidator : AbstractValidator<OrdersUpdateDto>
    {
        public OrdersUpdateDtoValidator()
        {
            RuleFor(o => o.CustomerId).MaximumLength(5);
            RuleFor(o => o.ShipName).MaximumLength(40);
            RuleFor(o => o.ShipAddress).MaximumLength(60);
            RuleFor(o => o.ShipCity).MaximumLength(15);
            RuleFor(o => o.ShipCountry).MaximumLength(15);
            RuleFor(o => o.ShipPostcode).MaximumLength(10);
        }
    }
}
