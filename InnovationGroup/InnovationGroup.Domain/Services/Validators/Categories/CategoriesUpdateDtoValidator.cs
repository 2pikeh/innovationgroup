﻿using FluentValidation;
using InnovationGroup.Domain.Categories;

namespace InnovationGroup.Domain.Services.Validators.Categories
{
    public class CategoriesUpdateDtoValidator : AbstractValidator<CategoriesUpdateDto>
    {
        public CategoriesUpdateDtoValidator()
        {
            RuleFor(c => c.Name).NotEmpty().Length(5, 15);
            RuleFor(c => c.Id).GreaterThan(0);
        }
    }
}
