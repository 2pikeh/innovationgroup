﻿using FluentValidation;
using InnovationGroup.Domain.Categories;

namespace InnovationGroup.Domain.Services.Validators.Categories
{
    public class CategoriesCreateDtoValidator : AbstractValidator<CategoriesCreateDto>
    {
        public CategoriesCreateDtoValidator()
        {
            RuleFor(c => c.Name).NotEmpty().Length(5, 15);
        }
    }
}
