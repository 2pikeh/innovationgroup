﻿using System.Collections.Generic;
using InnovationGroup.Domain.Uri;

namespace InnovationGroup.Domain.Services
{
    public interface IResourceUriService
    {
        string CreateResourceUri(string routeName, dynamic routeValues);

        string CreateResourceUri(ResourceQueryParameters queryParameters, ResourceUriType type,
            string routeName);

        IEnumerable<UriDto> CreateResourceUri(ResourceQueryParameters queryParameters, bool hasPreviousPage,
            bool hasNextPage, string routeName);

        IEnumerable<UriDto> CreateResourceUri(string id, IEnumerable<RouteUriMetadata> routeConfiguration);

        IEnumerable<RouteUriMetadata> GenerateCustomerRoutes(string customerId, ResourceQueryParameters queryParameters);

        IEnumerable<RouteUriMetadata> GenerateEmployeeRoutes(int employeeId, ResourceQueryParameters queryParameters);

        IEnumerable<RouteUriMetadata> GenerateOrderRoutes(int orderId, string customerId,
            ResourceQueryParameters queryParameters);

        IEnumerable<RouteUriMetadata> GenerateCategoriesRoutes(int categoryId, ResourceQueryParameters queryParameters);
    }
}
