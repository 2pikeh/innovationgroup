﻿namespace InnovationGroup.Domain.Services
{
    public class PagingMetadataWithNavigationUris : PaginationMetadataBase
    {
        public PagingMetadataWithNavigationUris(int pageSize, int currentPage, int totalPages, int totalCount, string prevPageUri, string nextPageUri)
        : base(pageSize, currentPage, totalPages, totalCount)
        {
            PreviousPageUri = prevPageUri;
            NextPageUri = nextPageUri;
        }

        public string PreviousPageUri { get; }
        public string NextPageUri { get; }
    }
}
