﻿namespace InnovationGroup.Domain.Uri
{
    public class RouteUriMetadata
    {
        public RouteUriMetadata(string routeName, dynamic routeValues, string routeReference, string routeMethod)
        {
            RouteName = routeName;
            RouteValues = routeValues;
            RouteReference = routeReference;
            RouteMethod = routeMethod;
        }

        public string RouteName { get; }
        public dynamic RouteValues { get; }
        public string RouteReference { get; }
        public string RouteMethod { get; }
    }
}
