﻿using System.Collections.Generic;

namespace InnovationGroup.Domain.Uri
{
    public class CollectionResource
    {
        public CollectionResource(IEnumerable<IDictionary<string, object>> value)
        {
            Value = value;
        }
        public IEnumerable<IDictionary<string, object>> Value { get; }
    }
}
