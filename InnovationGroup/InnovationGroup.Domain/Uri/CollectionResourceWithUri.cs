﻿using System.Collections.Generic;

namespace InnovationGroup.Domain.Uri
{
    public class CollectionResourceWithUri : CollectionResource
    {
        public CollectionResourceWithUri(IEnumerable<IDictionary<string, object>> value, IEnumerable<UriDto> uris)
            : base(value)
        {
            Uris = uris;
        }

        public IEnumerable<UriDto> Uris { get; }
    }
}
