﻿namespace InnovationGroup.Domain.Uri
{
    public enum ResourceUriType
    {
        NextPage,
        PreviousPage,
        CurrentPage
    }
}
