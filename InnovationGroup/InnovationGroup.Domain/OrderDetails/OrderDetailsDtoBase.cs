﻿namespace InnovationGroup.Domain.OrderDetails
{
    public abstract class OrderDetailsDtoBase
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public decimal UnitPrice { get; set; }
        public short Quantity { get; set; }
        public float Discount { get; set; }
    }
}
