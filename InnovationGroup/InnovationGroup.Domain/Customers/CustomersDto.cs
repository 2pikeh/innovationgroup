﻿using System.Collections.Generic;
using InnovationGroup.Domain.Orders;

namespace InnovationGroup.Domain.Customers
{
    public class CustomersDto : CustomersDtoBase
    {
        public ICollection<OrdersDto> Orders { get; set; }
    }
}
