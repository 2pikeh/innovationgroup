﻿using System.ComponentModel.DataAnnotations;

namespace InnovationGroup.Domain.Customers
{
    public abstract class CustomersDtoBase
    {
        public string Id { get; set; }
        [Required]
        public string Company { get; set; }

        public string ContactName { get; set; }

        public string ContactTitle { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }
    }
}
