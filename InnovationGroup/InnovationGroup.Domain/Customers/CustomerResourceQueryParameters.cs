﻿using InnovationGroup.Domain.Services;

namespace InnovationGroup.Domain.Customers
{
    public class CustomerResourceQueryParameters : ResourceQueryParameters
    {
        public string NavigationPropertiesToInclude { get; set; }
    }
}
