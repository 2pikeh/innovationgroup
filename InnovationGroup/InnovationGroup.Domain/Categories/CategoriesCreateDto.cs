﻿namespace InnovationGroup.Domain.Categories
{
    public class CategoriesCreateDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public byte[] Picture { get; set; }
    }
}
