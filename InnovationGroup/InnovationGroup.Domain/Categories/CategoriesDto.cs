﻿using System.Collections.Generic;
using InnovationGroup.Domain.Products;

namespace InnovationGroup.Domain.Categories
{
    public class CategoriesDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public byte[] Picture { get; set; }
        public virtual ICollection<ProductsDto> Products { get; set; }
    }
}
