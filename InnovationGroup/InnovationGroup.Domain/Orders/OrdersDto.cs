﻿using System;
using System.Collections.Generic;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.OrderDetails;

namespace InnovationGroup.Domain.Orders
{
    public class OrdersDto
    {
        public int Id { get; set; }
        public string CustomerId { get; set; }

        public DateTime? OrderDate { get; set; }

        public DateTime? RequiredDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        public decimal? ShippingCost { get; set; }

        public string ShipName { get; set; }

        public string ShipAddress { get; set; }

        public string ShipCity { get; set; }

        public string ShipRegion { get; set; }

        public string ShipPostcode { get; set; }

        public string ShipCountry { get; set; }

        public virtual CustomersDto Customer { get; set; }

        public virtual EmployeesDto Employee { get; set; }

        public virtual ICollection<OrderDetailsDto> OrderDetails { get; set; }

    }
}
