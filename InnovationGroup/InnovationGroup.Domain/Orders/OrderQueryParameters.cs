﻿using InnovationGroup.Domain.Services;

namespace InnovationGroup.Domain.Orders
{
    public class OrderQueryParameters : ResourceQueryParameters
    {
        public bool LoadOrderDetails { get; set; }
    }
}
