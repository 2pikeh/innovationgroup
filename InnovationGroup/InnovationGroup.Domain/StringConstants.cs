﻿namespace InnovationGroup.Domain
{
    public class StringConstants
    {
        public const string CustomMediaFormatter = "application/vnd.innovationgroup.hateoas+json";

        // Categories
        public const string GetCategoryById = nameof(GetCategoryById);
        public const string GetAllCategories = nameof(GetAllCategories);
        public const string AddNewCategory = nameof(AddNewCategory);
        public const string ExecuteDeepUpdateOnCategory = nameof(ExecuteDeepUpdateOnCategory);
        public const string ExecutePartialUpdateOnCategory = nameof(ExecutePartialUpdateOnCategory);
        public const string DeleteCategory = nameof(DeleteCategory);

        // Employees
        public const string GetAllEmployees = nameof(GetAllEmployees);
        public const string GetEmployeeById = nameof(GetEmployeeById);
        public const string AddNewEmployee = nameof(AddNewEmployee);
        public const string DeleteEmployee = nameof(DeleteEmployee);
        public const string ExecuteDeepUpdateOnEmployee = nameof(ExecuteDeepUpdateOnEmployee);
        public const string ExecutePartialUpdateOnEmployee = nameof(ExecutePartialUpdateOnEmployee);

        // Orders
        public const string GetAllOrders = nameof(GetAllOrders);
        public const string GetOrderById = nameof(GetOrderById);
        public const string AddNewOrder = nameof(AddNewOrder);
        public const string DeleteOrder = nameof(DeleteOrder);
        public const string GetOrderForCustomer = nameof(GetOrderForCustomer);
        public const string ExecuteDeepUpdateOnOrder = nameof(ExecuteDeepUpdateOnOrder);
        public const string ExecutePartialUpdateOnOrder = nameof(ExecutePartialUpdateOnOrder);
        public const string GetAllOrdersForCustomer = nameof(GetAllOrdersForCustomer);
        public const string AddNewOrderForCustomer = nameof(AddNewOrderForCustomer);

        // Customers
        public const string AddNewCustomer = nameof(AddNewCustomer);
        public const string GetAllCustomers = nameof(GetAllCustomers);
        public const string GetCustomerById = nameof(GetCustomerById);
        public const string GetCustomerOrder = nameof(GetCustomerOrder);
        public const string GetCustomerOrders = nameof(GetCustomerOrders);
        public const string DeleteCustomer = nameof(DeleteCustomer);
        public const string ExecuteDeepUpdateOnCustomer = nameof(ExecuteDeepUpdateOnCustomer);
        public const string ExecutePartialUpdateOnCustomer = nameof(ExecutePartialUpdateOnCustomer);
    }
}
