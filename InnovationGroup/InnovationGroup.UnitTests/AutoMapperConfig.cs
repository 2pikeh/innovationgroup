﻿using AutoMapper;

namespace InnovationGroup.UnitTests
{
    /// <summary>
    /// Sets up auto-mapper support in unit test project
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// Bootstraps auto-mapper type transformations for the unit
        /// test project.
        /// </summary>
        /// <returns></returns>
        public static IMapper Initialise()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfile>());
            return mapperConfiguration.CreateMapper();
        }
    }
}
