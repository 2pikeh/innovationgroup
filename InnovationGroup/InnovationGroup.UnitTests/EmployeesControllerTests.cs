﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using InnovationGroup.Controllers;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Employees;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using Xunit;
using ResourceQueryParameters = InnovationGroup.Domain.Services.ResourceQueryParameters;

namespace InnovationGroup.UnitTests
{
    public class EmployeesControllerTests 
    {
        private readonly AutoMocker _autoMocker;
        private readonly List<Employee> _items;

        public EmployeesControllerTests()
        {
            _autoMocker = new AutoMocker();
            _items = GetEmployees();

            SetupRepositoryMock();
            SetupServicesMock(currentPage: 1, pageSize: 3);
        }

        [Fact]
        public async Task Should_Return_Employee_Resource_Collection()
        {
            // Arrange
            var employeeController = _autoMocker.CreateInstance<EmployeesController>();

            // Act
            var response = await employeeController.GetAllEmployees(new ResourceQueryParameters(), "application/json");
            var okObjectResult = response as OkObjectResult;

            // Assert
            okObjectResult.Should().NotBeNull();

            var employees = (okObjectResult?.Value as CollectionResource)?.Value?.ToArray();
            var items = _items.ToArray();

            Assert.True(employees != null, nameof(employees) + " != null");

            for (var i = 0; i < employees.Length; i++)
            {
                employees[i][nameof(EmployeesDto.Id)].Should().Be(items[i].EmployeeId);
                employees[i][nameof(EmployeesDto.Name)].Should().Be($"{items[i].FirstName} {items[i].LastName}");
                employees[i][nameof(EmployeesDto.Notes)].Should().Be(items[i].Notes);
            }

            employees.Length.Should().Be(_items.Count);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(i => i.GetAll(It.IsAny<ResourceQueryParameters>()), Times.Once);

            _autoMocker.GetMock<IPropertyMappingService>()
                .Verify(i => i.GetPropertyMapping<object, object>(), Times.Never);

            _autoMocker.GetMock<IPagedDatasetService>()
                .Verify(
                    i => i.GetPagingMetadata(It.IsAny<PagedDataset<Employee>>(),
                        It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Given_a_Custom_HATEOAS_Accept_Header_Should_Return_HATEOAS_Employee_Resource_Response()
        {
            // Arrange
            var employeeController = _autoMocker.CreateInstance<EmployeesController>();

            // Act
            var response = await employeeController.GetAllEmployees(new ResourceQueryParameters(), StringConstants.CustomMediaFormatter);
            var okObjectResult = response as OkObjectResult;

            // Assert
            okObjectResult.Should().NotBeNull();

            var okObjectValue = okObjectResult?.Value as CollectionResourceWithUri;

            okObjectValue.Should().NotBeNull();

            var items = _items.ToArray();
            var employees = okObjectValue?.Value.ToArray();
            var uriCollection = okObjectValue?.Uris;

            Assert.True(uriCollection != null, nameof(uriCollection) + " != null");
            Assert.True(employees != null, nameof(employees) + " != null");

            uriCollection.Any().Should().BeTrue();           
            employees.Length.Should().Be(_items.Count);

            for (var i = 0; i < employees.Length; i++)
            {
                ((IEnumerable<UriDto>)employees[i][nameof(CollectionResourceWithUri.Uris)]).Any().Should().BeTrue();
                ((IEnumerable<UriDto>)employees[i][nameof(CollectionResourceWithUri.Uris)]).Count().Should().Be(CreateResourceUris().Count);

                employees[i][nameof(EmployeesDto.Id)].Should().Be(items[i].EmployeeId);
                employees[i][nameof(EmployeesDto.Name)].Should().Be($"{items[i].FirstName} {items[i].LastName}");
                employees[i][nameof(EmployeesDto.Notes)].Should().Be(items[i].Notes);
            }

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(i => i.GetAll(It.IsAny<ResourceQueryParameters>()), Times.Once);

            _autoMocker.GetMock<IPagedDatasetService>()
                .Verify(
                    i => i.GetPagingMetadata(It.IsAny<PagedDataset<Employee>>(),
                        It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<ResourceQueryParameters>(), It.IsAny<bool>(), It.IsAny<bool>(),
                        It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<ExpandoObject>>()),
                    Times.Never);
        }

        [Fact]
        public async Task Given_a_Valid_EmployeeId_Should_Return_an_Employee_Resource()
        {
            // Arrange
            const int idToFind = 2;
            var employeeController = _autoMocker.CreateInstance<EmployeesController>();

            // Act
            var response =
                await employeeController.GetEmployeeById(idToFind, new ResourceQueryParameters(),
                    "application/json");

            var okObjectResult = response as OkObjectResult;
            var employee = okObjectResult?.Value as IDictionary<string, object>;

            // Assert
            Assert.True(okObjectResult != null);
            Assert.True(employee != null, nameof(employee) + " != null");

            employee[nameof(EmployeesDto.Id)].Should().Be(idToFind);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(i => i.GetByIdAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<ExpandoObject>>()),
                    Times.Never);
        }

        [Fact]
        public async Task Given_a_Valid_Payload_Should_Create_Employee_Resource()
        {
            // Arrange
            var employeeCount = _items.Count;
            var employeeDto = new EmployeesCreateDto { FirstName = "Desc 1 ", LastName = "update" };

            var employeeController = _autoMocker.CreateInstance<EmployeesController>();

            // Act
            var response = await employeeController.AddNewEmployee(employeeDto);
            var createdAtRouteResult = response as CreatedAtRouteResult;
            var newlyCreatedEmployee = createdAtRouteResult?.Value as EmployeesDto;

            // Assert
            Assert.True(newlyCreatedEmployee != null);
            newlyCreatedEmployee.Id.Should().Be(employeeCount + 1);

            createdAtRouteResult.Should().NotBeNull();
            _items.Count.Should().Be(employeeCount + 1);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(repo => repo.CreateAsync(It.IsAny<Employee>()), Times.Once);
            
        }

        [Fact]
        public async Task Given_an_Invalid_Payload_Creating_New_Employee_Resource_Should_Fail_Validation()
        {
            // Arrange
            var employee = default(EmployeesCreateDto);
            var employeeController = _autoMocker.CreateInstance<EmployeesController>();

            // Act
            var response = await employeeController.AddNewEmployee(employee);
            var badRequestResult = response as BadRequestResult;

            // Assert
            badRequestResult.Should().NotBeNull();
        }

        [Fact]
        public async Task Given_Valid_EmployeeId_Should_Remove_Employee_Resource()
        {
            // Arrange
            var empToDelete = 3;
            var employeeCount = _items.Count;
            var employeeController = _autoMocker.CreateInstance<EmployeesController>();

            // Act
            var response = await employeeController.DeleteEmployee(empToDelete);
            var noContentResult = response as NoContentResult;

            // Assert
            noContentResult.Should().NotBeNull();
            _items.Count.Should().Be(employeeCount - 1);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(repo => repo.EmployeeExistsAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(repo => repo.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Given_Valid_Payload_All_Properties_Of_Employee_Resource_Should_Be_Updated()
        {
            // Arrange
            var employeeController = _autoMocker.CreateInstance<EmployeesController>();
            var toUpdate = new EmployeesUpdateDto {EmployeeId = 1, FirstName = "Desc 1 ", LastName= "update" };

            // Act
            var employeeId = toUpdate.EmployeeId;
            var response = await employeeController.ExecuteDeepUpdate(employeeId, toUpdate);
            var noContentResult = response as NoContentResult;

            // Assert
            noContentResult.Should().NotBeNull();
            _items.Find(c => c.EmployeeId == employeeId)
                .FirstName.Should()
                .Be(toUpdate.FirstName);

            noContentResult.Should().NotBeNull();
            _items.Find(c => c.EmployeeId == employeeId)
                .LastName.Should()
                .Be(toUpdate.LastName);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(repo => repo.EmployeeExistsAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(repo => repo.GetByIdAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<IEmployeesRepository>()
                .Verify(repo => repo.Update(It.IsAny<Employee>()), Times.Once);
        }

        private void SetupServicesMock(int currentPage, int pageSize)
        {
            _autoMocker.Use(AutoMapperConfig.Initialise());
            _autoMocker.GetMock<IPagedDatasetService>()
                .Setup(p => p.ApplyPaging(It.IsAny<IQueryable<Employee>>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new PagedDataset<Employee>(_items, _items.Count, currentPage, pageSize))
                .Verifiable();

            _autoMocker.GetMock<IPagedDatasetService>()
                .Setup(p => p.GetPagingMetadata(It.IsAny<PagedDataset<Employee>>(),
                    It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()))
                .Returns(new PagingMetadataWithNavigationUris(3, 1, 1, _items.Count, "http://localhost/prev",
                    "http://localhost/next"))
                .Verifiable();

            _autoMocker.GetMock<IResourceUriService>()
                .Setup(r => r.CreateResourceUri(It.IsAny<ResourceQueryParameters>(), It.IsAny<bool>(), It.IsAny<bool>(),
                    It.IsAny<string>()))
                .Returns(CreateResourceUris)
                .Verifiable();

            _autoMocker.GetMock<IResourceUriService>()
                .Setup(r => r.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<RouteUriMetadata>>()))
                .Returns(CreateResourceUris)
                .Verifiable();

            _autoMocker.GetMock<IPropertyMappingService>()
                .Setup(r => r.ValidMappingExistsFor<object, object>(It.IsAny<string>()))
                .Returns(true)
                .Verifiable();

            _autoMocker.GetMock<IPropertyMappingService>()
                .Setup(r => r.GetPropertyMapping<object, object>())
                .Returns(PropertyMappingTables.EmployeePropertyMapping)
                .Verifiable();

            _autoMocker.GetMock<ITypeHelperService>()
                .Setup(r => r.TypeHasProperties<object>(It.IsAny<string>()))
                .Returns(true)
                .Verifiable();
        }

        private void SetupRepositoryMock()
        {
            _autoMocker.GetMock<IEmployeesRepository>()
                .Setup(c => c.GetAll(It.IsAny<ResourceQueryParameters>()))
                .Returns(new PagedDataset<Employee>(_items, _items.Count, 1, 3))
                .Verifiable();

            _autoMocker.GetMock<IEmployeesRepository>()
                .Setup(c => c.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(_items.FirstOrDefault(c => c.EmployeeId == id)))
                .Verifiable();

            _autoMocker.GetMock<IEmployeesRepository>()
                .Setup(c => c.DeleteAsync(It.IsAny<int>()))
                .Returns<int>(id =>
                {
                    var employee = _items.Find(c => c.EmployeeId == id);
                    _items.Remove(employee);

                    return Task.FromResult(1);
                })
                .Verifiable();

            _autoMocker.GetMock<IEmployeesRepository>()
                .Setup(c => c.EmployeeExistsAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(_items.Any(c => c.EmployeeId == id)))
                .Verifiable();

            _autoMocker.GetMock<IEmployeesRepository>()
                .Setup(c => c.CreateAsync(It.IsAny<Employee>()))
                .Returns<Employee>(c =>
                {
                    c.EmployeeId = (_items.Count + 1);
                    _items.Add(c);

                    return Task.FromResult(1);
                })
                .Verifiable();

            _autoMocker.GetMock<IEmployeesRepository>()
                .Setup(c => c.Update(It.IsAny<Employee>()))
                .Returns<Employee>(toUpdate =>
                {
                    var fromDb = _items.Find(c => c.EmployeeId == toUpdate.EmployeeId);
                    _items.Remove(fromDb);
                    _items.Add(toUpdate);

                    return Task.FromResult(1);
                })
                .Verifiable();
        }

        private static List<Employee> GetEmployees()
        {
            return new List<Employee>
            {
                new Employee {EmployeeId = 1, FirstName = "Desc 1", LastName ="L1", Notes="Notes 1"},
                new Employee {EmployeeId = 2, FirstName = "Desc 2", LastName ="L2", Notes="Notes 2"},
                new Employee {EmployeeId = 3, FirstName = "Desc 3", LastName ="L3", Notes="Notes 3"},
                new Employee {EmployeeId = 4, FirstName = "Desc 4", LastName ="L4", Notes="Notes 4"},
            };
        }

        private static List<UriDto> CreateResourceUris()
        {
            return new List<UriDto>
            {
                new UriDto("http://localhost/self", "self", "GET"),
                new UriDto("http://localhost/previous", "previousPage", "GET"),
                new UriDto("http://localhost/next", "nextPage", "GET")
            };
        }
    }
}
