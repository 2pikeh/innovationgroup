﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using InnovationGroup.Controllers;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Customers;
using InnovationGroup.Domain.Services;
using InnovationGroup.Domain.Uri;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using Xunit;

namespace InnovationGroup.UnitTests
{
    public class CustomerControllerTests
    {
        private readonly AutoMocker _autoMocker;
        private readonly List<Customer> _items;

        public CustomerControllerTests()
        {
            _autoMocker = new AutoMocker();
            _items = SetupTestCustomerData();

            SetupRepositoryMock();
            SetupServicesMock(currentPage: 1, pageSize: 3);
        }

        [Fact]
        public void Should_Return_Customer_Resource_Collection()
        {
            // Arrange
            var customerController = _autoMocker.CreateInstance<CustomersController>();

            // Act
            var response = customerController.GetCustomers(new CustomerResourceQueryParameters(), "application/json");
            var okObjectResult = response as OkObjectResult;

            // Assert
            okObjectResult.Should().NotBeNull();

            var customers = (okObjectResult?.Value as CollectionResource)?.Value?.ToArray();
            var items = _items.ToArray();

            Assert.True(customers != null, nameof(customers) + " != null");

            for (var i = 0; i < customers.Length; i++)
            {
                customers[i][nameof(CustomersDto.Id)].Should().Be(items[i].CustomerId);
                customers[i][nameof(CustomersDto.ContactTitle)].Should().Be(items[i].ContactTitle);
                customers[i][nameof(CustomersDto.ContactName)].Should().Be(items[i].ContactName);
            }

            customers.Length.Should().Be(_items.Count);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(i => i.GetAll(It.IsAny<CustomerResourceQueryParameters>()), Times.Once);

            _autoMocker.GetMock<IPropertyMappingService>()
                .Verify(i => i.GetPropertyMapping<object, object>(), Times.Never);

            _autoMocker.GetMock<IPagedDatasetService>()
                .Verify(
                    i => i.GetPagingMetadata(It.IsAny<PagedDataset<Customer>>(),
                        It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void Given_a_Custom_HATEOAS_Accept_Header_Should_Return_HATEOAS_Customer_Resource_Response()
        {
            // Arrange
            var customer = _autoMocker.CreateInstance<CustomersController>();

            // Act
            var response = customer.GetCustomers(new CustomerResourceQueryParameters(), StringConstants.CustomMediaFormatter);
            var okObjectResult = response as OkObjectResult;

            // Assert
            okObjectResult.Should().NotBeNull();

            var okObjectValue = okObjectResult?.Value as CollectionResourceWithUri;

            okObjectValue.Should().NotBeNull();

            var items = _items.ToArray();
            var customers = okObjectValue?.Value.ToArray();
            var uriCollection = okObjectValue?.Uris;

            Assert.True(uriCollection != null, nameof(uriCollection) + " != null");
            Assert.True(customers != null, nameof(customers) + " != null");

            uriCollection.Any().Should().BeTrue();
            customers.Length.Should().Be(_items.Count);

            for (var i = 0; i < customers.Length; i++)
            {
                ((IEnumerable<UriDto>)customers[i][nameof(CollectionResourceWithUri.Uris)]).Any().Should().BeTrue();
                ((IEnumerable<UriDto>)customers[i][nameof(CollectionResourceWithUri.Uris)]).Count().Should().Be(CreateResourceUris().Count);

                customers[i][nameof(CustomersDto.Id)].Should().Be(items[i].CustomerId);
                customers[i][nameof(CustomersDto.ContactName)].Should().Be(items[i].ContactName);
                customers[i][nameof(CustomersDto.ContactTitle)].Should().Be(items[i].ContactTitle);
            }

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(i => i.GetAll(It.IsAny<CustomerResourceQueryParameters>()), Times.Once);

            _autoMocker.GetMock<IPagedDatasetService>()
                .Verify(
                    i => i.GetPagingMetadata(It.IsAny<PagedDataset<Customer>>(),
                        It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<ResourceQueryParameters>(), It.IsAny<bool>(), It.IsAny<bool>(),
                        It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<ExpandoObject>>()),
                    Times.Never);
        }

        [Fact]
        public async Task Given_a_Valid_CustomerId_Should_Return_a_Customer_Resource()
        {
            // Arrange
            const string idToFind = "{7F5A5092-6E6D-4B1A-8B51-16B6FBD870A1}";
            var customerController = _autoMocker.CreateInstance<CustomersController>();

            // Act
            var response =
                await customerController.GetCustomerById(idToFind, new CustomerResourceQueryParameters(),
                    "application/json");

            var okObjectResult = response as OkObjectResult;
            var customer = okObjectResult?.Value as IDictionary<string, object>;

            // Assert
            Assert.True(okObjectResult != null);
            Assert.True(customer != null, nameof(customer) + " != null");

            customer[nameof(CustomersDto.Id)].Should().Be(idToFind);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(i => i.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<ExpandoObject>>()),
                    Times.Never);
        }

        [Fact]
        public async Task Given_a_Valid_Payload_Should_Create_Customer_Resource()
        {
            // Arrange
            var customerCount = _items.Count;
            var customerDto = new CustomersCreateDto { Id = Guid.NewGuid().ToString(), ContactName = "Some name", ContactTitle = "Title"};

            var customerController = _autoMocker.CreateInstance<CustomersController>();

            // Act
            var response = await customerController.CreateNewCustomerEntry(customerDto);
            var createdAtRouteResult = response as CreatedAtRouteResult;
            var createdCustomer = createdAtRouteResult?.Value as CustomersDto;

            // Assert
            Assert.True(createdCustomer != null);
            createdCustomer.Id.Should().Be(customerDto.Id);

            createdAtRouteResult.Should().NotBeNull();
            _items.Count.Should().Be(customerCount + 1);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(repo => repo.CreateAsync(It.IsAny<Customer>()), Times.Once);

        }

        [Fact]
        public async Task Given_an_Invalid_Payload_Creating_New_Customer_Resource_Should_Fail_Validation()
        {
            // Arrange
            var customer = default(CustomersCreateDto);
            var customerController = _autoMocker.CreateInstance<CustomersController>();

            // Act
            var response = await customerController.CreateNewCustomerEntry(customer);
            var badRequestResult = response as BadRequestResult;

            // Assert
            badRequestResult.Should().NotBeNull();
        }

        [Fact]
        public async Task Given_Valid_CustomerId_Should_Remove_Customer_Resource()
        {
            // Arrange
            var customerToDelete = "{6788A2D2-D079-4814-9871-4C09BE596C42}";
            var customerCount = _items.Count;
            var customerController = _autoMocker.CreateInstance<CustomersController>();

            // Act
            var response = await customerController.DeleteCustomer(customerToDelete);
            var noContentResult = response as NoContentResult;

            // Assert
            noContentResult.Should().NotBeNull();
            _items.Count.Should().Be(customerCount - 1);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(repo => repo.CustomerExistsAsync(It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(repo => repo.DeleteAsync(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Given_Valid_Payload_All_Properties_Of_Customer_Resource_Should_Be_Updated()
        {
            // Arrange
            var customerController = _autoMocker.CreateInstance<CustomersController>();
            var toUpdate = new CustomersUpdateDto { Id = "{F3DC73EC-4EF9-441E-BDCE-A43B9FD166F8}", ContactName = "new contact", ContactTitle = "Lord."};

            // Act
            var customerId = toUpdate.Id;
            var response = await customerController.ExecuteDeepUpdate(customerId, toUpdate);
            var noContentResult = response as NoContentResult;

            // Assert
            noContentResult.Should().NotBeNull();
            _items.Find(c => c.CustomerId == customerId).ContactName.Should().Be(toUpdate.ContactName);

            noContentResult.Should().NotBeNull();
            _items.Find(c => c.CustomerId == customerId).ContactTitle.Should().Be(toUpdate.ContactTitle);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(repo => repo.CustomerExistsAsync(It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(repo => repo.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<ICustomerRepository>()
                .Verify(repo => repo.Update(It.IsAny<Customer>()), Times.Once);
        }

        private void SetupServicesMock(int currentPage, int pageSize)
        {
            _autoMocker.Use(AutoMapperConfig.Initialise());
            _autoMocker.GetMock<IPagedDatasetService>()
                .Setup(p => p.ApplyPaging(It.IsAny<IQueryable<Customer>>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new PagedDataset<Customer>(_items, _items.Count, currentPage, pageSize))
                .Verifiable();

            _autoMocker.GetMock<IPagedDatasetService>()
                .Setup(p => p.GetPagingMetadata(It.IsAny<PagedDataset<Customer>>(),
                    It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()))
                .Returns(new PagingMetadataWithNavigationUris(3, 1, 1, _items.Count, "http://localhost/prev",
                    "http://localhost/next"))
                .Verifiable();

            _autoMocker.GetMock<IResourceUriService>()
                .Setup(r => r.CreateResourceUri(It.IsAny<ResourceQueryParameters>(), It.IsAny<bool>(), It.IsAny<bool>(),
                    It.IsAny<string>()))
                .Returns(CreateResourceUris)
                .Verifiable();

            _autoMocker.GetMock<IResourceUriService>()
                .Setup(r => r.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<RouteUriMetadata>>()))
                .Returns(CreateResourceUris)
                .Verifiable();

            _autoMocker.GetMock<IPropertyMappingService>()
                .Setup(r => r.ValidMappingExistsFor<object, object>(It.IsAny<string>()))
                .Returns(true)
                .Verifiable();

            _autoMocker.GetMock<IPropertyMappingService>()
                .Setup(r => r.GetPropertyMapping<object, object>())
                .Returns(PropertyMappingTables.CustomersPropertyMapping)
                .Verifiable();

            _autoMocker.GetMock<ITypeHelperService>()
                .Setup(r => r.TypeHasProperties<object>(It.IsAny<string>()))
                .Returns(true)
                .Verifiable();
        }

        private void SetupRepositoryMock()
        {
            _autoMocker.GetMock<ICustomerRepository>()
                .Setup(c => c.GetAll(It.IsAny<CustomerResourceQueryParameters>()))
                .Returns(new PagedDataset<Customer>(_items, _items.Count, 1, 3))
                .Verifiable();

            _autoMocker.GetMock<ICustomerRepository>()
                .Setup(c => c.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns<string, string>((id, navProps) => Task.FromResult(_items.FirstOrDefault(c => c.CustomerId == id)))
                .Verifiable();

            _autoMocker.GetMock<ICustomerRepository>()
                .Setup(c => c.DeleteAsync(It.IsAny<string>()))
                .Returns<string>(id =>
                {
                    var customer = _items.Find(c => string.Equals(c.CustomerId, id, StringComparison.InvariantCultureIgnoreCase));
                    _items.Remove(customer);

                    return Task.FromResult(1);
                })
                .Verifiable();

            _autoMocker.GetMock<ICustomerRepository>()
                .Setup(c => c.CustomerExistsAsync(It.IsAny<string>()))
                .Returns<string>(id => Task.FromResult(_items.Any(c => c.CustomerId == id)))
                .Verifiable();

            _autoMocker.GetMock<ICustomerRepository>()
                .Setup(c => c.CreateAsync(It.IsAny<Customer>()))
                .Returns<Customer>(c =>
                {
                    _items.Add(c);

                    return Task.FromResult(1);
                })
                .Verifiable();

            _autoMocker.GetMock<ICustomerRepository>()
                .Setup(c => c.Update(It.IsAny<Customer>()))
                .Returns<Customer>(toUpdate =>
                {
                    var fromDb = _items.Find(c => c.CustomerId == toUpdate.CustomerId);
                    _items.Remove(fromDb);
                    _items.Add(toUpdate);

                    return Task.FromResult(1);
                })
                .Verifiable();
        }

        private static List<Customer> SetupTestCustomerData()
        {
            return new List<Customer>
            {
                new Customer { CustomerId = "{F3DC73EC-4EF9-441E-BDCE-A43B9FD166F8}", CompanyName = "Company1", ContactName = "Contact1", ContactTitle = "Ms"},
                new Customer { CustomerId = "{CF1417B1-94B8-4852-AEF0-D0AE16C10CE3}", CompanyName = "Company2", ContactName = "Contact2", ContactTitle = "Dr"},
                new Customer { CustomerId = "{6788A2D2-D079-4814-9871-4C09BE596C42}", CompanyName = "Company3", ContactName = "Contact3", ContactTitle = "Mr"},
                new Customer { CustomerId = "{7F5A5092-6E6D-4B1A-8B51-16B6FBD870A1}", CompanyName = "Company4", ContactName = "Contact4", ContactTitle = "Mrs"}
            };
        }

        private static List<UriDto> CreateResourceUris()
        {
            return new List<UriDto>
            {
                new UriDto("http://localhost/self", "self", "GET"),
                new UriDto("http://localhost/previous", "previousPage", "GET"),
                new UriDto("http://localhost/next", "nextPage", "GET")
            };
        }
    }
}
