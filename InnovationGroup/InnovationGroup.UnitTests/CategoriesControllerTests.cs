using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using InnovationGroup.Controllers;
using InnovationGroup.Data.DataModels;
using InnovationGroup.Data.Repositories.Interfaces;
using InnovationGroup.Data.Services;
using InnovationGroup.Domain;
using InnovationGroup.Domain.Categories;
using InnovationGroup.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using Xunit;
using InnovationGroup.Domain.Uri;

namespace InnovationGroup.UnitTests
{
    public class CategoriesControllerTests
    {
        private readonly AutoMocker _autoMocker;
        private readonly List<Category> _items;

        public CategoriesControllerTests()
        {
            _autoMocker = new AutoMocker();
            _items = GetCategories();

            SetupRepositoryMock();
            SetupServicesMock(currentPage:1, pageSize:3);
        }

        [Fact]
        public void Should_Return_Category_Resources()
        {
            // Arrange
            var categoryController = _autoMocker.CreateInstance<CategoriesController>();

            // Act
            var response = categoryController.GetAllCategories(new ResourceQueryParameters(), "application/json");
            var okObjectResult = response as OkObjectResult;

            // Assert
            Assert.True(okObjectResult != null);

            var categories = (okObjectResult.Value as CollectionResource)?.Value?.ToArray();
            var items = _items.ToArray();

            Assert.True(categories != null);

            for (var i = 0; i < categories.Length; i++)
            {
                categories[i][nameof(CategoriesDto.Id)].Should().Be(items[i].CategoryId);
                categories[i][nameof(CategoriesDto.Description)].Should().Be(items[i].Description);
            }

            categories.Length.Should().Be(_items.Count);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(i => i.GetAll(It.IsAny<ResourceQueryParameters>()), Times.Once);

            _autoMocker.GetMock<IPagedDatasetService>()
                .Verify(
                    i => i.GetPagingMetadata(It.IsAny<PagedDataset<Category>>(),
                        It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void Given_a_Custom_HATEOAS_Accept_Header_Should_Return_HATEOAS_Category_Resource_Response()
        {
            // Arrange
            var categoryController = _autoMocker.CreateInstance<CategoriesController>();

            // Act
            var response =
                categoryController.GetAllCategories(new ResourceQueryParameters(),
                    StringConstants.CustomMediaFormatter);
            var okObjectResult = response as OkObjectResult;
            var okObjectValue = okObjectResult?.Value as CollectionResourceWithUri;

            // Assert
            Assert.True(okObjectResult != null);
            Assert.True(okObjectValue != null);

            var items = _items.ToArray();
            var categories = okObjectValue?.Value.ToArray();
            var uris = okObjectValue?.Uris;

            uris.Any().Should().BeTrue();
            categories.Length.Should().Be(_items.Count);

            for (var i = 0; i < categories.Length; i++)
            {
                ((IEnumerable<UriDto>)categories[i][nameof(CollectionResourceWithUri.Uris)]).Any().Should().BeTrue();
                ((IEnumerable<UriDto>)categories[i][nameof(CollectionResourceWithUri.Uris)]).Count().Should().Be(CreateResourceUris().Count);
                categories[i][nameof(CategoriesDto.Id)].Should().Be(items[i].CategoryId);
                categories[i][nameof(CategoriesDto.Description)].Should().Be(items[i].Description);
            }

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(i => i.GetAll(It.IsAny<ResourceQueryParameters>()), Times.Once);

            _autoMocker.GetMock<IPagedDatasetService>()
                .Verify(
                    i => i.GetPagingMetadata(It.IsAny<PagedDataset<Category>>(),
                        It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<ResourceQueryParameters>(), It.IsAny<bool>(), It.IsAny<bool>(),
                        It.IsAny<string>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<ExpandoObject>>()), 
                    Times.Never);
        }

        [Fact]
        public async Task Given_a_CategoryId_Should_Return_a_Category_Resource()
        {
            // Arrange
            const int categoryIdToFind = 2;
            var categoryController = _autoMocker.CreateInstance<CategoriesController>();

            // Act
            var response =
                await categoryController.GetCategoryById(categoryIdToFind, new ResourceQueryParameters(),
                    "application/json");

            var okObjectResult = response as OkObjectResult;
            var category = okObjectResult?.Value as IDictionary<string, object>;

            // Assert
            Assert.True(okObjectResult != null);
            Assert.True(category != null);
            
            category[nameof(CategoriesDto.Id)].Should().Be(categoryIdToFind);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(i => i.GetByIdAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<IResourceUriService>()
                .Verify(
                    i => i.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<ExpandoObject>>()),
                    Times.Never);
        }

        [Fact]
        public async Task Should_Create_New_Category_Resource()
        {
            // Arrange
            var categoryCount = _items.Count;
            var category = new CategoriesCreateDto
            {
                Name = "Unit Test",
                Description = "Some desc from unit test",
                Picture = new byte[0]
            };

            var categoryController = _autoMocker.CreateInstance<CategoriesController>();

            // Act
            var response = await categoryController.AddNewCategory(category);
            var createdAtRouteResult = response as CreatedAtRouteResult;
            var newlyCreatedCategory = createdAtRouteResult?.Value as CategoriesDto;

            // Assert
            Assert.True(newlyCreatedCategory != null);
            newlyCreatedCategory.Id.Should().Be(categoryCount + 1);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(repo => repo.CreateAsync(It.IsAny<Category>()), Times.Once);

            createdAtRouteResult.Should().NotBeNull();
            _items.Count.Should().Be(categoryCount + 1);
        }

        [Fact]
        public async Task When_Creating_New_Category_With_Empty_Payload_Should_Fail_Validation()
        {
            // Arrange
            var category = default(CategoriesCreateDto);
            var categoryController = _autoMocker.CreateInstance<CategoriesController>();

            // Act
            var response = await categoryController.AddNewCategory(category);
            var badRequestResult = response as BadRequestResult;

            // Assert
            badRequestResult.Should().NotBeNull();
        }

        [Fact]
        public async Task Given_a_Valid_CategoryId_Should_Remove_Category_Resource()
        {
            // Arrange
            int categoryIdToDelete = 3;
            int categoryCount = _items.Count;
            var categoryController = _autoMocker.CreateInstance<CategoriesController>();

            // Act
            var response = await categoryController.DeleteCategory(categoryIdToDelete);
            var noContentResult = response as NoContentResult;

            // Assert
            noContentResult.Should().NotBeNull();
            _items.Count.Should().Be(categoryCount - 1);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(repo => repo.CategoryExistsAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(repo => repo.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Given_a_Valid_Payload_Should_Update_All_Fields_When_Performing_an_Update()
        {
            // Arrange
            var categoryController = _autoMocker.CreateInstance<CategoriesController>();
            var categoryToUpdate = new CategoriesUpdateDto
            {
                Id = 2,
                Name = "Full Update"
            };

            // Act
            var response = await categoryController.ExecuteDeepUpdate(categoryToUpdate.Id, categoryToUpdate);
            var noContentResult = response as NoContentResult;

            // Assert
            noContentResult.Should().NotBeNull();
            _items.Find(c => c.CategoryId == categoryToUpdate.Id)
                .CategoryName.Should()
                .Be(categoryToUpdate.Name);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(repo => repo.CategoryExistsAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(repo => repo.GetByIdAsync(It.IsAny<int>()), Times.Once);

            _autoMocker.GetMock<ICategoriesRepository>()
                .Verify(repo => repo.Update(It.IsAny<Category>()), Times.Once);
        }

        private void SetupServicesMock(int currentPage, int pageSize)
        {
            _autoMocker.Use(AutoMapperConfig.Initialise());
            _autoMocker.GetMock<IPagedDatasetService>()
                .Setup(p => p.ApplyPaging(It.IsAny<IQueryable<Category>>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new PagedDataset<Category>(_items, _items.Count, currentPage, pageSize))
                .Verifiable();

            _autoMocker.GetMock<IPagedDatasetService>()
                .Setup(p => p.GetPagingMetadata(It.IsAny<PagedDataset<Category>>(),
                    It.IsAny<ResourceQueryParameters>(), It.IsAny<string>()))
                .Returns(new PagingMetadataWithNavigationUris(3, 1, 1, _items.Count, "http://localhost/prev",
                    "http://localhost/next"))
                .Verifiable();

            _autoMocker.GetMock<IResourceUriService>()
                .Setup(r => r.CreateResourceUri(It.IsAny<ResourceQueryParameters>(), It.IsAny<bool>(), It.IsAny<bool>(),
                    It.IsAny<string>()))
                .Returns(CreateResourceUris)
                .Verifiable();

            _autoMocker.GetMock<IResourceUriService>()
                .Setup(r => r.CreateResourceUri(It.IsAny<string>(), It.IsAny<IEnumerable<RouteUriMetadata>>()))
                .Returns(CreateResourceUris)
                .Verifiable();

            _autoMocker.GetMock<IPropertyMappingService>()
                .Setup(r => r.ValidMappingExistsFor<object, object>(It.IsAny<string>()))
                .Returns(true)
                .Verifiable();

            _autoMocker.GetMock<IPropertyMappingService>()
                .Setup(r => r.GetPropertyMapping<object, object>())
                .Returns(PropertyMappingTables.CategoriesPropertyMapping)
                .Verifiable();

            _autoMocker.GetMock<ITypeHelperService>()
                .Setup(r => r.TypeHasProperties<object>(It.IsAny<string>()))
                .Returns(true)
                .Verifiable();
        }

        private void SetupRepositoryMock()
        {
            _autoMocker.GetMock<ICategoriesRepository>()
                .Setup(c => c.GetAll(It.IsAny<ResourceQueryParameters>()))
                .Returns(new PagedDataset<Category>(_items, _items.Count, 1, 3))
                .Verifiable();

            _autoMocker.GetMock<ICategoriesRepository>()
                .Setup(c => c.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(_items.FirstOrDefault(c => c.CategoryId == id)))
                .Verifiable();

            _autoMocker.GetMock<ICategoriesRepository>()
                .Setup(c => c.DeleteAsync(It.IsAny<int>()))
                .Returns<int>(id =>
                {
                    var category = _items.Find(c => c.CategoryId == id);
                    _items.Remove(category);

                    return Task.FromResult(1);
                })
                .Verifiable();

            _autoMocker.GetMock<ICategoriesRepository>()
                .Setup(c => c.CategoryExistsAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(_items.Any(c => c.CategoryId == id)))
                .Verifiable();

            _autoMocker.GetMock<ICategoriesRepository>()
                .Setup(c => c.CreateAsync(It.IsAny<Category>()))
                .Returns<Category>(c =>
                {
                    c.CategoryId = _items.Count + 1;
                    _items.Add(c);
                    return Task.FromResult(1);
                })
                .Verifiable();

            _autoMocker.GetMock<ICategoriesRepository>()
                .Setup(c => c.Update(It.IsAny<Category>()))
                .Returns<Category>(toUpdate =>
                {
                    var fromDb = _items.Find(c => c.CategoryId == toUpdate.CategoryId);
                    _items.Remove(fromDb);
                    _items.Add(toUpdate);

                    return Task.FromResult(1);
                })
                .Verifiable();
        }

        private static List<Category> GetCategories()
        {
            return new List<Category>
            {
                new Category {CategoryId = 1, CategoryName = "One", Description = "Desc One"},
                new Category {CategoryId = 2, CategoryName = "Two", Description = "Desc Two"},
                new Category {CategoryId = 3, CategoryName = "Three", Description = "Desc Three"}
            };
        }

        private static List<UriDto> CreateResourceUris()
        {
            return new List<UriDto>
            {
                new UriDto("http://localhost/self", "self", "GET"),
                new UriDto("http://localhost/previous", "previousPage", "GET"),
                new UriDto("http://localhost/next", "nextPage", "GET")
            };
        }
    }
}
